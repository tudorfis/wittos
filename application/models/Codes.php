<?php

class Model_Codes extends Zend_Db_Table_Row_Abstract {
    
    /*   
        CREATE TABLE IF NOT EXISTS `codes` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `code` varchar(255) DEFAULT NULL,
          `status` enum('accepted','rejected','pending') DEFAULT 'pending',
          `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT= 1;
    */
            
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->_data["id"];
    }
    
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    public function getCode()
    {
        return $this->code;
    }
    
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }
    
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
        return $this;
    }

    public function getDateCreated()
    {
        return $this->date_created;
    }
    
}