<?php

class Model_Pages extends Zend_Db_Table_Row_Abstract {

    /*     
        CREATE TABLE IF NOT EXISTS `pages` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `title` varchar(255) DEFAULT NULL,
          `content` text,
          `image_id` int(11) DEFAULT NULL,
          `category_id` int(11) DEFAULT NULL,
          `gallery_id` int(11) DEFAULT NULL,
          `resource_id` int(11) DEFAULT NULL,
          `static_id` int(11) DEFAULT NULL,
          `facebook_id` varchar(255) DEFAULT NULL,
          `keyword` varchar(255) DEFAULT NULL,
          `ord` int(11) DEFAULT NULL,
          `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
          `page_type` enum('static','facebook') NOT NULL DEFAULT 'static',
          `slug` varchar(255) DEFAULT NULL,
          `date_created` datetime NOT NULL,
          `date_modified` datetime NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ; 
    */

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->_data["id"];
    }
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setImageId($image_id)
    {
        $this->image_id = $image_id;
        return $this;
    }

    public function getImageId()
    {
        return $this->image_id;
    }

    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
        return $this;
    }

    public function getCategoryId()
    {
        return $this->category_id;
    }

    public function setGalleryId($gallery_id)
    {
        $this->gallery_id = $gallery_id;
        return $this;
    }

    public function getGalleryId()
    {
        return $this->gallery_id;
    }

    public function setResourceId($resource_id)
    {
        $this->resource_id = $resource_id;
        return $this;
    }

    public function getResourceId()
    {
        return $this->resource_id;
    }

    public function setStaticId($static_id)
    {
        $this->static_id = $static_id;
        return $this;
    }

    public function getStaticId()
    {
        return $this->static_id;
    }

    public function setFacebookId($facebook_id)
    {
        $this->facebook_id = $facebook_id;
        return $this;
    }

    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    public function setKeyword($keyword)
    {
        $this->keyword = strtolower($keyword);
        return $this;
    }

    public function getKeyword()
    {
        return strtolower($this->keyword);
    }

    public function setOrd($ord)
    {
        $this->ord = $ord;
        return $this;
    }

    public function getOrd()
    {
        return $this->ord;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setPageType($page_type)
    {
        $this->page_type = $page_type;
        return $this;
    }

    public function getPageType()
    {
        return $this->page_type;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
        return $this;
    }

    public function getDateCreated()
    {
        return $this->date_created;
    }

    public function setDateModified($date_modified)
    {
        $this->date_modified = $date_modified;
        return $this;
    }

    public function getDateModified()
    {
        return $this->date_modified;
    }

}
    