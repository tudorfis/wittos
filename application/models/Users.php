<?php

class Model_Users extends Zend_Db_Table_Row_Abstract {
    
    /*
    CREATE TABLE IF NOT EXISTS `users` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `username` varchar(255) COLLATE utf8_bin NOT NULL,
      `password` varchar(255) COLLATE utf8_bin NOT NULL,
      `first_name` varchar(40) COLLATE utf8_bin DEFAULT NULL,
      `last_name` varchar(40) COLLATE utf8_bin DEFAULT NULL,
      `role` enum('user','admin') COLLATE utf8_bin DEFAULT NULL,
      `fb_id` varchar(20) COLLATE utf8_bin DEFAULT NULL,
      `status` enum('enabled','disabled','suspended') COLLATE utf8_bin DEFAULT 'disabled',
      `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`),
      UNIQUE KEY `ukEmail` (`username`),
      KEY `kPassword` (`password`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;
    */

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->_data["id"];
    }

    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    public function getUsername()
    {
        return $this->username;
    }
    
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }
    
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
        return $this;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }
    
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
        return $this;
    }

    public function getLastName()
    {
        return $this->last_name;
    }
    
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    public function getRole()
    {
        return $this->role;
    }
    
    public function setFbId($fb_id)
    {
        $this->fb_id = $fb_id;
        return $this;
    }

    public function getFbId()
    {
        return $this->fb_id;
    }
    
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }
    
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
        return $this;
    }

    public function getDateCreated()
    {
        return $this->date_created;
    }

    public function getFullName(){
        return $this->_data['first_name'].' '.$this->_data['last_name'];
    }

}
