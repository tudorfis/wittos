<?php

class Model_Categories extends Zend_Db_Table_Row_Abstract {

    /*     
     CREATE TABLE IF NOT EXISTS `categories` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `zone_id` int(11) NOT NULL,
          `title` varchar(255) NOT NULL,
          `description` text,
          `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
          `ord` int(11) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
    */

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->_data["id"];
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }
    
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }
    
    public function setZoneId($zone_id)
    {
        $this->zone_id = $zone_id;
        return $this;
    }

    public function getZoneId()
    {
        return $this->zone_id;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

}
