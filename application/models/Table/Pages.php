<?php

class Table_Pages  extends Table_Abstract {

    const PAGE_TYPE_STATIC = "static";
    const PAGE_TYPE_FACEBOOK = "facebook";
    
    const STATUS_ACTIVE = "active";
    const STATUS_INACTIVE = "inactive";
    const STATUS_DELETED = "deleted";
    
    protected $_name = 'pages';
    protected $_rowClass = 'Model_Pages';
    protected $_image;
    
    public function getById($id){

        $select = $this->select()
                    ->from(array("p"=>$this->_name))
                    ->where("id = ?",$id);

        return $this->fetchRow($select);
    }
    
    public function getBySlug($slug){

        $select = $this->select()
                    ->from(array("p"=>$this->_name))
                    ->where("slug = ?",$slug);

        return $this->fetchRow($select);
    }
    
    public function getByFacebookId($facebook_id){

        $select = $this->select()
                    ->from(array("p"=>$this->_name))
                    ->where("status <> '".self::STATUS_DELETED."'")
                    ->where("facebook_id = '".$facebook_id."'");

        return $this->fetchRow($select);
    }
    
    public function getAllByCategoryId($category_id){

        $select = $this->select()
                    ->from(array("p"=>$this->_name))
                    ->where("category_id = ?",$category_id)
                    ->where("p.status = ?",self::STATUS_ACTIVE);

        return $this->fetchAll($select);
    }
    
    public function getAll($filter = null)
    {
        if( ! is_array($filter)  ){
            $filter = array();
        }

        $basicSelect = $this->select()
            ->setIntegrityCheck(false)
            ->from(array("p"=>$this->_name));
        
        if(!empty($filter["page_type"]) ){
            $basicSelect->where("p.page_type = ?",$filter["page_type"]);
        } 
            
        if(!empty($filter["status"]) ){
            $basicSelect->where("p.status = ?",$filter["status"]);
        } else {
            $basicSelect->where("p.status <> '".self::STATUS_DELETED."'");
        }
        
        if ( !empty($filter['order']) ) {
            $basicSelect->order($filter['order']);
        }

        return $this->fetchAll($basicSelect);

    }
    
    public function getMaxOrd() 
    {
        $stmt = $this->_db->query('select MAX(ord) from `'.$this->_name.'`');
        $stmt->execute();
        return implode($stmt->fetch());
    }
                                             
}
