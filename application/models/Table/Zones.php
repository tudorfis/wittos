<?php

class Table_Zones  extends Table_Abstract {

    const STATUS_ACTIVE = "active";
    const STATUS_INACTIVE = "inactive";
    const STATUS_DELETED = "deleted";
    
    protected $_name = 'zones';
    protected $_rowClass = 'Model_Zones';
    
    public function getById($id){

        $select = $this->select()
                    ->where("id = ?",$id);

        return $this->fetchRow($select);
    }
    
    public function getAll($filter = null)
    {

        if( ! is_array($filter)  ){
            $filter = array();
        }

        $basicSelect = $this->select()
            ->setIntegrityCheck(false)
            ->from(array("z"=>$this->_name));
            
        if ( !empty($filter['order']) ) {
            $basicSelect->order($filter['order']);
        }
        
        if(!empty($filter["status"]) ){
            $basicSelect->where("z.status = ?",$filter["status"]);
        } else {
            $basicSelect->where("z.status <> '".self::STATUS_DELETED."'");
        }

        return $this->fetchAll($basicSelect);

    }

}
