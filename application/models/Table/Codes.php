<?php

class Table_Codes  extends Table_Abstract {

    const STATUS_ACCEPTED = "accepted";
    const STATUS_REJECTED = "rejected";
    const STATUS_PENDING = "pending";
    
    protected $_name = 'codes';
    protected $_rowClass = 'Model_Codes';
    
    public function getById($id){

        $select = $this->select()
                    ->where("id = ?",$id);

        return $this->fetchRow($select);
    }
    
    public function getByCode($code){

        $select = $this->select()
                    ->where("code = ?",$code);

        return $this->fetchRow($select);
    }
    
    public function getAll($filter = null){

        if( ! is_array($filter)  ){
            $filter = array();
        }

        $basicSelect = $this->select()
            ->setIntegrityCheck(false)
            ->from(array("c"=>$this->_name));

        if(!empty($filter["status"]) ){
            $basicSelect->where("c.status = ?",$filter["status"]);
        }
        
        if (!empty($filter["date_created"])) {
            $basicSelect->where("date(date_created) = ?", $filter["date_created"]);
        }

        return $this->fetchAll($basicSelect);

    }

}
