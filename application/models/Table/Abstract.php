<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hpsese
 * Date: 08/06/11
 * Time: 18:14
 * To change this template use File | Settings | File Templates.
 */
 
abstract class Table_Abstract extends Zend_Db_Table {

    protected $count;
    protected $pages;
    protected $limit;


    public function __construct($config = array(), $definition = null){
        parent::__construct($config,$definition);

        $config = Zend_Registry::get("__CONFIG__");

        if ( isset( $config["page"]["limit"] ) ){
            $this->limit = $config["page"]["limit"];
        }
        else{
            $this->limit = 20;
        }
    }

    public function setCount($count)
    {
        $this->count = $count;
        return $this;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return intVal( $this->count );
    }

    public function getPages()
    {
        $this->pages = ceil($this->count / $this->limit);
        return $this->pages;
    }

    public function setLimit($limit)
    {
        if( ! is_null( $limit ) ){
            $this->limit = $limit;
        }
        return $this;
    }

    public function getLimit()
    {
        return $this->limit;
    }

}
