<?php

class Table_Files  extends Table_Abstract {

    const FILE_TYPE_STATIC = 'static';
    const FILE_TYPE_FACEBOOK = 'facebook';
    
    const STATUS_ACTIVE = "active";
    const STATUS_INACTIVE = "inactive";
    const STATUS_DELETED = "deleted";

    protected $_name = 'files';
    protected $_rowClass = 'Model_Files';

    public function getById($id){

        $select = $this->select()
                    ->where("id = ?",$id);

        return $this->fetchRow($select);
    }
    

}
