<?php

class Table_Gallery  extends Table_Abstract {

    const STATUS_ACTIVE = "active";
    const STATUS_INACTIVE = "inactive";
    const STATUS_DELETED = "deleted";
    
    protected $_name = 'gallery';
    protected $_rowClass = 'Model_Gallery';

    public function getById($id){

        $select = $this->select()
                    ->where("id = ?",$id);

        return $this->fetchRow($select);
    }
    
    public function getAll($filter = null) {

        if( ! is_array($filter)  ){
            $filter = array();
        }

        $basicSelect = $this->select()
            ->setIntegrityCheck(false)
            ->from(array("g"=>$this->_name));

        if(!empty($filter["status"]) ){
            $basicSelect->where("g.status = ?",$filter["status"]);
        } else {
            $basicSelect->where("g.status <> '".self::STATUS_DELETED."'");
        }

        return $this->fetchAll($basicSelect);

    }
    

}
