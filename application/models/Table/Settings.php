<?php

class Table_Settings  extends Table_Abstract {

    protected $_name = 'settings';
    protected $_rowClass = 'Model_Settings';


    public function getById($id){

        $select = $this->select()
                    ->where("id = ?",$id);

        return $this->fetchRow($select);
    }
    
    public function getByName($name){

        $select = $this->select()
                    ->where("name = ?",$name);

        return $this->fetchRow($select);
    }

}
