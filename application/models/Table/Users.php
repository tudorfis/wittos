<?php
 
class Table_Users  extends Table_Abstract {


    const ROLE_GUEST = "guest" ;
    const ROLE_USER = "user" ;
    const ROLE_ADMIN = "admin" ;

    const STATUS_ENABLED = "enabled";
    const STATUS_DISABLED = "disabled";
    const STATUS_SUSPENDED = "suspended";

    protected $_name = 'users';
    protected $_rowClass = 'Model_Users';


    /**
     * @param $username
     * @param $password
     * @return null|Model_User
     */
    public function getAuthUsernamePassword( $username, $password ){

        $select = $this->select()
                    ->where("username = ?",$username)
                    ->where("md5(?) = password",$password);

        return $this->fetchRow($select);
    }


    /**
     * @param $id
     * @return null|Model_User
     */
    public function getById($id){

        $select = $this->select()
                    ->where("id = ?",$id);


        return $this->fetchRow($select);
    }

    /**
     * @param $fb_id
     * @return null|Model_User
     */
    public function getByFacebookId( $fb_id ){
        $select = $this->select()
                    ->where("fb_id = ?",$fb_id );

        return $this->fetchRow($select);
    }

    /**
     * @param $username
     * @return null|Model_User
     */
    public function getByUsername($username){

        $select = $this->select()
                    ->where("username = ?",$username);


        return $this->fetchRow($select);
    }

    /**
     * Get All records using Filter, Pagination and Sort filter
     * @param $filter
     * @param int $page
     * @param array $sort
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getAll(&$filter, $page = 0, $sort=array()){

        $basicSelect = $this->select()
            ->setIntegrityCheck(false)
            ->from(array("u" => $this->_name));

        if( ! empty($filter["username"]) ){
            $basicSelect->where("username = ?",$filter["username"]);
        }

        if( ! empty($filter["password"]) ){
            $basicSelect->where("password = ?",$filter["password"]);
        }

        if( ! empty($filter["role"]) ){
            $basicSelect->where("role = ?",$filter["role"]);
        }

        if( $page ){
            if( ! $this->count ){
                $select = clone $basicSelect;
                $select->reset(Zend_Db_Select::COLUMNS );
                $select->columns(array('count'=>'COUNT(*)'));

                $this->count = $this->getAdapter()->fetchOne($select);
            }

            $basicSelect->limitPage($page,$this->limit);
        }

        if( ! empty($sort["method"]) ){
            $basicSelect->order($sort["order"]." ".$sort["method"]);
        }

        return $this->fetchAll($basicSelect);
    }


    /**
     * Delete records use list of ids
     * @param $ids
     * @return bool
     */
    public function deleteByIds( $ids ){

        if( is_null( $ids ) ){
            return false;
        }

        $select = $this->select()
            ->where("id in (?)",$ids);

        $rows = $this->fetchAll($select);

        if( ! $cnt = $rows->count() ){
            return false;
        }

        /**@var $user Model_User */
        foreach( $rows as $user){
            try{
                $user->delete();
            }
            catch( Exception $e ){
                NL_Log_Me::Log("User Delete Error: ".$e->getMessage(),Zend_Log::CRIT);
                return false;
            }
        }

        return true;
    }

}
