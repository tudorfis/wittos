<?php

class Table_Resources  extends Table_Abstract {

    const STATUS_ACTIVE = "active";
    const STATUS_INACTIVE = "inactive";
    const STATUS_DELETED = "deleted";
    
    const FEED_TYPE_FACEBOOK = 'facebook';
    const FEED_TYPE_TWITTER = 'twitter';
    const FEED_TYPE_WEBSITE = 'website';
    
    const FEED_LANG_EN = 'en';
    const FEED_LANG_FR = 'fr';
    const FEED_LANG_ANY = 'any';

    const VIDEO_SHOW_TRUE = '1';
    const VIDEO_SHOW_FALSE = '0';
              
    protected $_name = 'resources';
    protected $_rowClass = 'Model_Resources';

    public function getById($id) {

        $select = $this->select()
                    ->where("id = ?",$id);

        return $this->fetchRow($select);
    }
    
    public function getAll($filter = null) 
    {

        if( ! is_array($filter)  ){
            $filter = array();
        }

        $basicSelect = $this->select()
            ->setIntegrityCheck(false)
            ->from(array("g"=>$this->_name));

        if(!empty($filter["status"]) ){
            $basicSelect->where("g.status = ?",$filter["status"]);
        } else {
            $basicSelect->where("g.status <> '".self::STATUS_DELETED."'");
        }

        return $this->fetchAll($basicSelect);

    }
    
    public function getByKeyword($keyword) 
    {
        $stmt = $this->_db->query("select * from `".$this->_name."` 
            where `status` = '".self::STATUS_ACTIVE."' and `keyword` like '%".strtolower($keyword)."%'");
        $stmt->execute();
        return $stmt->fetch();
    }
    
    public function getByCategoryIdAndKeyword($category_id, $keyword) 
    {
        $stmt = $this->_db->query("select * from `".$this->_name."` 
            where `status` = '".self::STATUS_ACTIVE."' and
                    `category_id` = '$category_id' and
                    `keyword` like '%".strtolower($keyword)."%'");
        $stmt->execute();
        return $stmt->fetch();
    }
    
    
    

}