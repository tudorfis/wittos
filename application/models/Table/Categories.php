<?php

class Table_Categories  extends Table_Abstract {

    const STATUS_ACTIVE = "active";
    const STATUS_INACTIVE = "inactive";
    const STATUS_DELETED = "deleted";
    
    protected $_name = 'categories';
    protected $_rowClass = 'Model_Categories';
    
    public function getById($id){

        $select = $this->select()
                    ->where("id = ?",$id);

        return $this->fetchRow($select);
    }
    
    public function getAllByZoneId($zone_id){

        $select = $this->select()
                    ->from(array("c"=>$this->_name))
                    ->where("zone_id = ?",$zone_id)
                    ->where("c.status = ?",self::STATUS_ACTIVE);

        return $this->fetchAll($select);
    }
    
    public function getAll($filter = null)
    {

        if( ! is_array($filter)  ){
            $filter = array();
        }

        $basicSelect = $this->select()
            ->setIntegrityCheck(false)
            ->from(array("c"=>$this->_name));
            
        if ( !empty($filter['order']) ) {
            $basicSelect->order($filter['order']);
        }
        
        if(!empty($filter["status"]) ){
            $basicSelect->where("c.status = ?",$filter["status"]);
        } else {
            $basicSelect->where("c.status <> '".self::STATUS_DELETED."'");
        }

        return $this->fetchAll($basicSelect);

    }

}
