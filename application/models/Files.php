<?php

class Model_Files extends Zend_Db_Table_Row_Abstract {
    
    /*
    CREATE TABLE IF NOT EXISTS `files` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `path` varchar(255) NOT NULL,
      `file_type` enum('static','facebook') NOT NULL DEFAULT 'static',
      `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
      `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`),
      UNIQUE KEY `path` (`path`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
    );*/

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->_data["id"];
    }

    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    public function getPath()
    {
        return $this->path;
    }
    
    public function setPathThumb($path_thumb)
    {
        $this->path_thumb = $path_thumb;
        return $this;
    }

    public function getPathThumb()
    {
        return $this->path_thumb;
    }
    
    

    public function setFileType($file_type)
    {
        $this->file_type = $file_type;
        return $this;
    }

    public function getFileType()
    {
        return $this->file_type;
    }
    
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
        return $this;
    }

    public function getDateCreated()
    {
        return $this->date_created;
    }



}
