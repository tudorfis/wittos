<?php

class Model_Gallery extends Zend_Db_Table_Row_Abstract {
    
    /*
        CREATE TABLE IF NOT EXISTS `gallery` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `images` varchar(255) NOT NULL,
          `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
          `date_created` datetime NOT NULL,
          `date_modified` datetime NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
    */

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->_data["id"];
    }
    
    public function setImages($images)
    {
        $this->images = $images;
        return $this;
    }

    public function getImages()
    {
        return $this->images;
    }
    
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }
    
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
        return $this;
    }

    public function getDateCreated()
    {
        return $this->date_created;
    }

    public function setDateModified($date_modified)
    {
        $this->date_modified = $date_modified;
        return $this;
    }

    public function getDateModified()
    {
        return $this->date_modified;
    }

}
