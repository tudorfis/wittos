<?php
      
class Model_Zones extends Zend_Db_Table_Row_Abstract {
  
  /*     
   CREATE TABLE IF NOT EXISTS `zones` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `name` varchar(255) NOT NULL,
      `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
      `ord` int(11) NOT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
  */
        
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->_data["id"];
    }
    
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }
    
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }
    
    public function setOrd($ord)
    {
        $this->ord = $ord;
        return $this;
    }

    public function getOrd()
    {
        return $this->ord;
    }
    
}