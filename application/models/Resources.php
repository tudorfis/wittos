<?php

class Model_Resources extends Zend_Db_Table_Row_Abstract {

    /*
    CREATE TABLE IF NOT EXISTS `resources` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `keyword` varchar(255) DEFAULT NULL,
      `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'inactive',
      `link` varchar(255) DEFAULT NULL,
      `title` varchar(255) DEFAULT NULL,
      `description` text,
      `category_id` int(11) DEFAULT NULL,
      `feed_type` enum('facebook','twitter','website') DEFAULT NULL,
      `feed_lang` enum('en','fr','any') DEFAULT 'any',
      `video_show` tinyint(1) NOT NULL DEFAULT '0',
      `video_link` varchar(255) DEFAULT NULL,
      `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
    */
    
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
   
    public function getId()
    {
        return $this->_data["id"];
    }
    
    public function setKeyword($keyword)
    {
        $this->keyword = strtolower($keyword);
        return $this;
    }

    public function getKeyword()
    {
        return strtolower($this->keyword);
    }
    
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }
    
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    public function getLink()
    {
        return $this->link;
    }
    
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }
    
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }
    
    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
        return $this;
    }

    public function getCategoryId()
    {
        return $this->category_id;
    }
    
    public function setFeedType($feed_type)
    {
        $this->feed_type = $feed_type;
        return $this;
    }

    public function getFeedType()
    {
        return $this->feed_type;
    }
    
    public function setFeedLang($feed_lang)
    {
        $this->feed_lang = $feed_lang;
        return $this;
    }

    public function getFeedLang()
    {
        return $this->feed_lang;
    }
    
    public function setVideoShow($video_show)
    {
        $this->video_show = $video_show;
        return $this;
    }

    public function getVideoShow()
    {
        return $this->video_show;
    }
    
    public function setVideoLink($video_link)
    {
        $this->video_link = $video_link;
        return $this;
    }

    public function getVideoLink()
    {
        return $this->video_link;
    }
    
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
        return $this;
    }

    public function getDateCreated()
    {
        return $this->date_created;
    }
    
}
