<?php

return array(
    "debug" => false, // if true will be dumped to logme.log
    "roles" => array(
        // role => parent
        "guest" => null,
        "user" => "guest",
        "admin" => "user",
    ),
    "rules" => array(
        "allow" => array(
            // role => ( resource => privileges )
            "guest" => array(
                "default:index" => null,
                "default:auth" => null
            ),
            "admin" => array(
                "default:admin" => null,
                "default:account" => null,
            )
        )
    )
);