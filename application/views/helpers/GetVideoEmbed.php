<?php 

class Zend_View_Helper_GetVideoEmbed extends Zend_View_Helper_Abstract
{

    protected  $_videoEmbed = ''; 

    /**
    * Transforms video URL into embeded html
    * 
    * @param mixed $video_link - the link of video
    * @param mixed $width 
    * @param mixed $height 
    */
    public function getVideoEmbed($video_link = null, $width = 500, $height = 300)
	{
        if (!is_null($video_link)) 
        {
            $this->page['resource']['video_show'] = Table_Resources::VIDEO_SHOW_TRUE;
            preg_match('/((\.|\/)youtube\.com)/', $video_link, $matchesYoutube);
            preg_match('/((\.|\/)vimeo\.com)/', $video_link, $matchesVimeo);
            if (!empty($matchesYoutube)) {
                preg_match('/watch\?v\=(.{11})/', $video_link, $matchesId);
                $this->_videoEmbed = '<iframe width="'.$width.'" height="'.$height.'" src="//www.youtube.com/embed/'.$matchesId[1].'" frameborder="0" allowfullscreen></iframe>';  
            } elseif (!empty($matchesVimeo)) {
                preg_match('/vimeo\.com\/(.{8})$/', $video_link, $matchesId);
                $this->_videoEmbed = '<iframe src="//player.vimeo.com/video/'.$matchesId[1].'?byline=0&amp;portrait=0&amp;badge=0&amp;color=0887ff" width="'.$width.'" height="'.$height.'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
            } else {
                $this->page['resource']['video_show'] = Table_Resources::VIDEO_SHOW_FALSE;
            }
        }
        return $this->_videoEmbed;
	}
}