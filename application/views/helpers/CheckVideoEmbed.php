<?php 

class Zend_View_Helper_CheckVideoEmbed extends Zend_View_Helper_Abstract
{
    
    protected $_showVideoEmbed = false;
    
    /**
    * Checks if video is youtube or vimeo
    * 
    * @param mixed $video_link - the link of video
    * @param mixed $width 
    * @param mixed $height 
    */
    public function checkVideoEmbed($video_link = null)
    {
        if (!is_null($video_link)) 
        {
            preg_match('/((\.|\/)youtube\.com)/', $video_link, $matchesYoutube);
            preg_match('/((\.|\/)vimeo\.com)/', $video_link, $matchesVimeo);
            
            if (!empty($matchesYoutube)) {
               $this->_showVideoEmbed = true; 
            } elseif (!empty($matchesVimeo)) {
               $this->_showVideoEmbed = true;
            }
        }
        return $this->_showVideoEmbed;
    }
}