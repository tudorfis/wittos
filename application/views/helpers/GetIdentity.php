<?php 

class Zend_View_Helper_GetIdentity
{

    protected  $_identity = false; // not null !

    /**
     * Return null or identity
     *
     * @return mixed|null
     */
    function getIdentity()
	{

        if( $this->_identity === false ){
            $this->_identity = Zend_Auth::getInstance()->getIdentity();
        }

        return $this->_identity;

	}
}