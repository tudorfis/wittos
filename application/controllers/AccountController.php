<?php

    class AccountController extends NL_Controller_DefaultAction
    {

        public function init()
        {
            parent::init();
            $this->_helper->layout()->setLayout('admin');
        }

        public function indexAction()
        {
            $userTable = new Table_Users();
            $user = $userTable->getById($this->identityId);

            if( $this->getRequest()->isPost() ){

                $params = $this->getRequest()->getParams();
                $user->setFirstName($params["first_name"]);
                $user->setLastName($params["last_name"]);

                try{
                    $user->save();
                    $this->_helper->viewRenderer('/message/account-changed');
                }
                catch( Zend_Db_Exception $e ){
                    throw new Exception($e->getMessage(),501);
                }
            }

            $this->view->assign($user->toArray());
        }
        
        public function changePasswordAction()
        {
            $userTable = new Table_Users();
            $user = $userTable->getById($this->identityId);

            if( $this->getRequest()->isPost() ){

                $password = $this->getRequest()->getParam("password");
                $user->setPassword(md5($password));

                try{
                    $user->save();
                    $this->_helper->viewRenderer('/message/password-changed');
                }
                catch( Zend_Db_Exception $e ){
                    throw new Exception($e->getMessage(),501);
                }
            }

            $this->view->assign($user->toArray());
        }

        public function checkPasswordAction()
        {
            $this->disableLayout()->disableView();
            $password = $this->getRequest()->getParam("password");
            $id = $this->getRequest()->getParam("id");
            $userTable = new Table_Users();
            $user = $userTable->getById($id);
            print Zend_Json::encode($user->getPassword() == md5($password));
        }


    }

