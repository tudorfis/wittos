<?php

    class AuthController extends NL_Controller_DefaultAction
    {
        public function init()
        {
            $this->_helper->layout()->setLayout('admin');
        }

        public function indexAction()
        {
            if($this->getRequest()->isPost())
            {
                $auth = Zend_Auth::getInstance();
                $username = $this->getRequest()->getParam("username");
                $password = $this->getRequest()->getParam("password");
                $authAdapter = new NL_AuthAdapter();
                $authAdapter->setUsernamePass($username,$password);
                $result = $auth->authenticate($authAdapter);

                if( $result->isValid() ){
                    $identityId = $auth->getIdentity()->id;
                    if( $this->getRequest()->getParam("rememberMe") ) {
                        NL_AuthAdapter::saveCookie($identityId);
                    }
                    $this->_redirect("/admin");
                }

                $errorMessage = $result->getMessages();
                $this->view->assign( "username" , $username);
                $this->view->assign( "errorMessage" , $errorMessage[0] );
                $this->view->assign( "errorCode" , $result->getCode() );
            }
        }

        public function logoutAction()
        {
            NL_AuthAdapter::clearCookie();
            $namespace = new Zend_Session_Namespace();
            unset($namespace);

            Zend_Auth::getInstance()->clearIdentity();
            $this->_helper->viewRenderer('/message/logout');
        }

        //    public function registerAction()
        //    {

        //        include_once 'Recaptcha/recaptchalib.php';
        //        $this->view->assign("recaptchaPublicKey",$this->config["recaptcha"]["publicKey"]);


        //        if( $this->getRequest()->isPost() ){
        //            $params = $this->getRequest()->getParams();

        //            $recaptcha = recaptcha_check_answer(
        //                $this->config["recaptcha"]["privateKey"],
        //                $_SERVER["REMOTE_ADDR"],
        //                $params["recaptcha_challenge_field"],
        //                $params["recaptcha_response_field"]);

        //            if( ! $recaptcha->is_valid ){
        //                $this->view->assign($params);
        //                $this->view->assign( "errorMessage" , "Invalid Recaptcha Code" );
        //                $this->view->assign( "recaptchaError" , $recaptcha->error );
        //                return;
        //            }

        //            $userTable = new Table_User();

        //            $user = $userTable->createRow();

        //            $user->setFirstName($params["first_name"]);
        //            $user->setLastName($params["last_name"]);

        //            $user->setEmail($params["email"]);

        //            $user->setRole("user");
        //            $user->setPassword(md5($params["password"]));

        //            try{
        //                $user->save();
        //            }
        //            catch( Exception $e ){
        //                throw new Exception( $e->getMessage(),501);
        //            }

        //            $token = "";
        //            $email = new NL_HtmlMailer();

        //            if( $email->sendToken( $user, $token ) ){
        //                $this->messageCenter("token-sent");
        //            }
        //            else{
        //                $this->messageCenter("token-exception",array("token" => $token));
        //            }
        //        }
        //    }

        //    public function validateEmailAction(){
        //        $this->disableLayout()->disableView();

        //        if( $this->getRequest()->isPost() ){
        //            $userTable = new Table_User();

        //            $id = $this->getRequest()->getParam("id");
        //            $email = $this->getRequest()->getParam("email");

        //            if( ! filter_var($email, FILTER_VALIDATE_EMAIL) ){
        //                print Zend_Json::encode(false);
        //                exit();
        //            }

        //            $user = $userTable->getByEmail($email);

        //            if( is_null( $user ) ){
        //                print Zend_Json::encode(true);
        //                exit();
        //            }

        //            if( ! empty( $id ) ){
        //                if( $id == $user->getId() ){
        //                    print Zend_Json::encode(true);
        //                    exit();
        //                }
        //            }
        //        }
        //        print Zend_Json::encode(false);
        //    }


        //    public function checkPasswordAction(){

        //        $this->disableLayout()->disableView();

        //        $password = $this->getRequest()->getParam("password");
        //        $id = $this->getRequest()->getParam("id");

        //        $userTable = new Table_User();

        //        $user = $userTable->getById($id);

        //        print Zend_Json::encode($user->getPassword() == md5($password));
        //    }


        //    public function activateAction(){

        //        Zend_Auth::getInstance()->clearIdentity();

        //        if ( ! $token = $this->getRequest()->getParam("token") ){
        //            throw new Exception("Token is missing ! Contact webmaster",501);
        //        }

        //        $params = NL_Crypt::Decrypt($token);

        //        $expire = $params["expire"];

        //        if( ! $id = $params["id"] ){
        //            throw new Exception("An error occurred sending Id ! Contact webmaster",501);
        //        }

        //        $userTable = new Table_User;
        //        $user = $userTable->getById( $id );

        //        if( is_null($user) ){
        //            throw new Exception("Your sign up record don't exists anymore ! Please register again",501);
        //        }

        //        if( time() > $expire ) {
        //            if( $userTable::STATUS_DISABLED == $user->getStatus() ){
        //                $user->delete();
        //                throw new Exception("Token expired, and your sign up record was deleted.",501);
        //            }
        //            throw new Exception("Token expired, please don't use this link again",501);
        //        }

        //        if( $userTable::STATUS_ENABLED == $user->getStatus() ){
        //            throw new Exception("Your account was already activated successfully",501);
        //        }

        //        if( $userTable::STATUS_SUSPENDED == $user->getStatus() ){
        //            throw new Exception("Your account was suspended. Please contact webmaster",501);
        //        }

        //        $user->setStatus($userTable::STATUS_ENABLED);
        //        try{
        //            $user->save();
        //        }
        //        catch( Zend_Db_Exception $e ){
        //            throw new Exception("Database Error. Please try again later. If error persists please contact us.",501);
        //        }

        //        $authAdapter = new NL_AuthAdapter();
        //        $authAdapter->setUserId($id);

        //        $auth = Zend_Auth::getInstance();
        //        $result = $auth->authenticate($authAdapter);

        //        if( ! $result->isValid()){
        //            throw new Exception("Authentication error. An error occurred when try to login with your credentials",501);
        //        }

        //        $this->view->assign( "status" , "ok");
        //    }


        //    public function forgotAction() {
        //        
        //        if( $this->getRequest()->isPost() ){
        //            $email = $this->getRequest()->getParam("email");

        //            $userTable = new Table_User;
        //            $user = $userTable->getByEmail($email);

        //            if( is_null( $user ) ){
        //                $this->view->assign( "errorMessage" , "email is not registered" );
        //                return;
        //            }

        //            $token = "";
        //            $email = new NL_HtmlMailer();
        //            if( $email->sendForgotPassToken($user,$token) ){

        //            }
        //            else{


        //            }
        //        }
        //    }

        //    public function changePasswordAction(){

        //        if( $this->getRequest()->isPost() ){

        //            $id = $this->getRequest()->getParam("id");
        //            $password = $this->getRequest()->getParam("password");

        //            $userTable = new Table_User();

        //            $user = $userTable->getById($id);

        //            if( is_null( $user) ){
        //                throw new Exception("Error retrieve sign up record ! Try again, if error persist contact webmaster",501);
        //            }

        //            $user->setPassword(md5($password));
        //            try{
        //                $user->save();
        //            }
        //            catch( Zend_Db_Exception $e ){
        //                throw new Exception("Database Error. Please try again later. If error persists please contact us.",501);
        //            }
        //            $authAdapter = new NL_AuthAdapter();
        //            $authAdapter->setUserId($id);

        //            $auth = Zend_Auth::getInstance();
        //            $result = $auth->authenticate($authAdapter);

        //            if( ! $result->isValid()){
        //                throw new Exception("Authentication error. An error occurred when try to login with your credentials",501);
        //            }

        //            $this->messageCenter("password-changed");
        //            return;
        //        }

        //        Zend_Auth::getInstance()->clearIdentity();

        //        if ( ! $token = $this->getRequest()->getParam("token") ){
        //            throw new Exception("Missing token. Cannot continue",501);
        //        }

        //        try{
        //            $params = NL_Crypt::Decrypt($token);
        //        }
        //        catch ( Exception $e ){
        //            throw new Exception("Error decoding token. Cannot continue",501);
        //        }

        //        $expire = $params["expire"];

        //        $id = $params["id"];

        //        $userTable = new Table_User;

        //        $user = $userTable->getById( $id );

        //        if( is_null($user) ){
        //            throw new Exception("Your sign up record not exists anymore ! Please register again",501);
        //        }

        //        if( time() > $expire ) {
        //            throw new Exception("Token expired. Please try again.",501);
        //        }

        //        if( $userTable::STATUS_DISABLED == $user->getStatus() ){
        //            throw new Exception("Your account is not activated. Please check the email with activation instruction",501);
        //        }

        //        if( $userTable::STATUS_SUSPENDED == $user->getStatus() ){
        //            throw new Exception("Your account was suspended. Please contact Webmaster",501);
        //        }

        //        $this->view->assign("id",$user->getId());
        //    }


    }

