<?php

class AdminController extends NL_Controller_Abstract 
{
    public function init()
    {
        $this->_helper->layout()->setLayout('admin');
    }

    public function indexAction()
    {
    }
    
    public function deleteAction()
    {
         $this->_helper->layout->disableLayout();
         $this->_helper->viewRenderer->setNoRender(true);
        
         $id = $this->getRequest()->getParam("id"); 
         $table = $this->getRequest()->getParam("table");
         
         if(isset($table)) 
         {
            $className = 'Table_'.ucfirst($table);
            $classObj = new $className;
            $tableRow = $classObj->getById($id);
               
             $tableRow->setStatus($className::STATUS_DELETED);
             $tableRow->save();
               
             $this->_redirect('/admin/'.$table);    
         }
    }
    
    public function codesAction()
    {
        $codesTable = new Table_Codes();
        $filter = array();
        $codesArray = array();
        
        $filterRequest = $this->getRequest()->getParam('filter');
        if (empty($filterRequest)) {
            $filterRequest = 'pending';
        }
        
        $statusRequest = $this->getRequest()->getParam('status');
        if(!empty($statusRequest)) {
            $idRequest = $this->getRequest()->getParam('id');
            $codesTable->update(
                array('status' => $statusRequest),
                array('id = ?' => $idRequest));
        }
        
        switch ($filterRequest) {
            case 'all':
                $codesArray = $codesTable->getAll($filter)->toArray(); 
                break;
            case 'today':
                $filter['date_created'] = date('Y-m-d');
                break;
            case 'accepted':
                $filter['status'] = 'accepted';
                break;
            case 'rejected':
                $filter['status'] = 'rejected';
                break;
            case 'pending':
                $filter['status'] = 'pending';
                break;
        }
        
        $codesArray = $codesTable->getAll($filter)->toArray();            
        
        $this->view->assign('codes', $codesArray);
        $this->view->assign('filter', $filterRequest);
        
    }
    
    public function settingsAction()
    {
        $settingsTable = new Table_Settings();
        $submitRequest = $this->getRequest()->getParam('submit');
        if (!empty($submitRequest)) {
            
            $result['COUNTER_NUMBER'] = $settingsTable->update(
                array('value' => $this->getRequest()->getParam('counter_number')),
                array("name = 'COUNTER_NUMBER'"));
            
            $result['SHOW_ZONE_ID'] = $settingsTable->update(
                array('value' => $this->getRequest()->getParam('show_zone_id')),
                array("name = 'SHOW_ZONE_ID'"));
            
            
            if (array_sum($result) > 0) {
                $this->view->assign('success', true);
            }
        }

        $counterNumber = $settingsTable->getByName('COUNTER_NUMBER')->toArray();
        $counterNumber = (int) $counterNumber['value'];
        $showZoneId = $settingsTable->getByName('SHOW_ZONE_ID')->toArray();
        $showZoneId = (int) $showZoneId['value'];
        
        $this->view->assign('counter_number', $counterNumber);
        $this->view->assign('show_zone_id', $showZoneId);
        $this->view->assign('zonesSelect', $this->_getZonesSelect());
        
    }
    
    public function resourcesAction()
    {      
          $resourcesTable = new Table_Resources();
          $categoriesTable = new Table_Categories();
          $resourcesArray = $resourcesTable->getAll();
          $resourcesResult = array();
          
          foreach ($resourcesArray as $resources) {
               $resources = $resources->toArray();
               $resources['category'] = !is_null($resources['category_id'])?$categoriesTable->getById($resources['category_id'])->toArray():array();
               $resourcesResult[] = $resources;
          }
          $this->view->assign('resources', $resourcesResult);
    }
    
    public function resourceModifyAction()
    {
        $resourcesTable = new Table_Resources();
        $params = $this->getRequest()->getParams();
        
        if (isset($params['id'])) {
            $resource = $resourcesTable->getById($params['id']);
            $this->view->assign('resource', $resource);
            $this->view->assign('id', $params['id']);
            $this->view->assign('type', 'edit');    
                
        } else {
            $resource = $resourcesTable->createRow();
            $this->view->assign('type', 'add');
        }

        if (!empty($params['submit'])) 
        {
            $resource->setStatus($params['status']);
            $resource->setCategoryId($params['category_id']);
            $resource->setFeedType($params['feed_type']);
            $resource->setFeedLang($params['feed_lang']);
            $resource->setKeyword($params['keyword']);
            $resource->setLink($params['link']);
            $resource->setTitle($params['title']);
            $resource->setDescription($params['description']);
            $resource->setVideoShow($params['video_show']);
            $resource->setVideoLink($params['video_link']);
            
            $resource->save();
            $this->_redirect('/admin/resources');
        }
        
        $this->view->assign('statusSelect', $this->_getStatusSelect());
        $this->view->assign('categoriesSelect', $this->_getCategoriesSelect());
        $this->view->assign('feedTypeSelect', $this->_getFeedTypeSelect());
        $this->view->assign('feedLangSelect', $this->_getFeedLangSelect());
    }
    
    public function pagesAction()
    {
       $dateFromReq = $this->getRequest()->getParam('dateFrom');
       $dateToReq = $this->getRequest()->getParam('dateTo');

       $dateFromTxt = isset($dateFromReq)?$dateFromReq:date('Y-m-d', strtotime('-7 days'));
       $dateToTxt = isset($dateFromReq)?$dateToReq:date('Y-m-d');    
       
       $this->view->assign('dateFromTxt', $dateFromTxt); 
       $this->view->assign('dateToTxt', $dateToTxt); 

       $pagesTable = new Table_Pages();
       $filesTable = new Table_Files();
       $categoriesTable = new Table_Categories();
       $zonesTable = new Table_Zones();
       $pagesFacebookResult = array();

       $filter['page_type'] = 'facebook'; 
       $filter['order'] = 'date_created desc'; 
       $pagesFacebook = $pagesTable->getAll($filter);

       foreach ($pagesFacebook as $page) {
           $page = $page->toArray();
           $page['image'] = !is_null($page['image_id'])?$filesTable->getById($page['image_id'])->toArray():array();
           $page['category'] =!is_null($page['category_id'])?$categoriesTable->getById($page['category_id'])->toArray():array();
           $page['zone'] = isset($page['category']['zone_id'])?$zonesTable->getById($page['category']['zone_id'])->toArray():array();
           $pagesFacebookResult[] = $page;
       }
       $this->view->assign('pages_facebook', $pagesFacebookResult);
    }
    
    public function pageModifyAction()
    {
        $pagesTable = new Table_Pages();
        $params = $this->getRequest()->getParams();
        
        if (isset($params['id'])) {
            $page = $pagesTable->getById($params['id']);
            $this->view->assign('page', $page);
            $this->view->assign('type', 'edit');
        } else {
            $page = $pagesTable->createRow();
            $this->view->assign('type', 'add');
        }
    
        if (!empty($params['submit'])) 
        {
            $page->setStatus($params['status']);
            $page->setTitle($params['title']);
            $page->setCategoryId($params['category_id']);
            $page->setKeyword($params['keyword']);
            $page->save();
            $this->_redirect('/admin/pages');
        }
        
        $this->view->assign('categoriesSelect', $this->_getCategoriesSelect());
        $this->view->assign('statusSelect', $this->_getStatusSelect());    
    }
    
    public function getFacebookFeedAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $request = $this->getRequest()->getParam('get-facebook');
        if (isset($request))
        {
            $dateFrom = $this->getRequest()->getParam('dateFrom');
            $dateTo = $this->getRequest()->getParam('dateTo');
            
            $filesTable = new Table_Files();
            $pagesTable = new Table_Pages();
            
            require_once('facebook-php-sdk-master/src/facebook.php');
            $fbConfig = new Zend_Config_Ini(APPLICATION_PATH.'/configs/config.ini', APPLICATION_ENV);
            $fbConfig = $fbConfig->toArray();
            $fbConfig = $fbConfig['facebook'];
            $facebook = new Facebook($fbConfig);
            
            $facebook = new Facebook($fbConfig);
            $pagefeed = $facebook->api("/" . $fbConfig['pageId'] . "/feed?since=$dateFrom&until=$dateTo");
            $maxOrd = $pagesTable->getMaxOrd();
            
            foreach($pagefeed['data'] as $post) {
                if ($post['type'] == 'photo') 
                {
                    $facebook_idRaw = explode('_', $post['id']);
                    $facebook_id = $facebook_idRaw[1];
                    $page = $pagesTable->getByFacebookId($facebook_id);
                    $date_created = (!empty($post['updated_time']))?date("Y-m-d H:m:s", (strtotime($post['updated_time']))):date("Y-m-d H:m:s", (strtotime($post['created_time'])));
                    
                    if ((is_null($page)) && (!empty($post['picture']))) {
                        $title = $post['name'];
                        $content = nl2br($post['message']);
                        
                        $picture_thumb = $post['picture'];
                        preg_match('/(.+)_s(.+)/',$picture_thumb, $pictureRaw);
                        $picture_big = $pictureRaw[1].'_n'.$pictureRaw[2];
                        
                        $image = $filesTable->createRow();
                        $image->setPath($picture_big);
                        $image->setPathThumb($picture_thumb);
                        $image->setFileType($filesTable::FILE_TYPE_FACEBOOK);
                        $image->setStatus($filesTable::STATUS_ACTIVE);
                        $image->setDateCreated($date_created);
                        $image_id = $image->save();
                        
                        $page = $pagesTable->createRow();
                        $page->setTitle($title);
                        $page->setContent($content);
                        $page->setImageId($image_id);
                        $page->setFacebookId($facebook_id);
                        $page->setDateCreated($date_created);
                        $page->setPageType($pagesTable::PAGE_TYPE_FACEBOOK);
                        $page->setStatus($pagesTable::STATUS_INACTIVE);
                        $page->setOrd(++$maxOrd);
                        $page->save();  
                    } 
                }
            }
            $this->_helper->redirector('pages', 'admin', 'default', array('dateFrom' => $dateFrom, 'dateTo' => $dateTo));
        } else {
            $this->_redirect('/admin/pages');   
        }
        
    }
    

}
