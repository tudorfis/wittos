<?php

class IndexController extends NL_Controller_Abstract
{
    
    public function preDispatch()
    {
        $this->_makeTopMenu();    
    }
    
    public function init()
    {
    }

    public function indexAction()
    {
        // FOR DEVELOPMENT PURPOSES            
        if ((string) $this->getRequest()->getParam('cp') != '1') {
            $this->redirect('http://www.wittos.com');
        }
        
        if ($this->_isWinner() == true) {
            $this->view->assign('isWinner', true);
            $this->view->assign('generatedCode', $this->_generateWinCode());
        }
        
        $this->view->assign('hideMenu', true);
        $this->view->assign('images',$this->_makeGallery(1)); // HARDCODAT

    }

    public function menuAction()
    {
        $pagesTable = new Table_Pages();
        $filesTable = new Table_Files();
        $categoriesTable = new Table_Categories();
        $zonesTable = new Table_Zones();
        $settingsTable = new Table_Settings();
        
        $showZoneId = $settingsTable->getByName('SHOW_ZONE_ID')->toArray();
        $showZoneId = (int) $showZoneId['value'];
        
        $filter = array('page_type' => 'facebook',
                        'order' => 'ord ASC',
                        'status' => 'active');
        $pages = $pagesTable->getAll($filter);
        $pagesArray = array();
        
        foreach ($pages as $page) {
            $page = $page->toArray();
            $page['image'] = $filesTable->getById($page['image_id'])->toArray();
            $page['category'] = $categoriesTable->getById($page['category_id'])->toArray();
            $page['zone'] = $zonesTable->getById($page['category']['zone_id'])->toArray();
            $pagesArray[] = $page;
        }
        
        $this->view->assign('pages', $pagesArray);
        $this->view->assign('show_zone_id', $showZoneId);
    }

    public function thankyouAction()
    {
        $aRequest = $this->getRequest()->getParam('a');
        $continue_url = (!empty($aRequest))?$aRequest:'http://www.motorsport.com';
        
        $pattern = '/https?:\/\//';
        preg_match($pattern, $continue_url, $matches);
        if (empty($matches)) {
            $continue_url = 'http://'.$continue_url;
        }
        
        $pagesTable = new Table_Pages();
        $filesTable = new Table_Files();
        $filter['status'] = 'active';
        $pagesArray = $pagesTable->getAll($filter)->toArray();
        $images = array();
        foreach($pagesArray as $page) {
            $images[$page['image_id']] = $filesTable->getById($page['image_id'])->toArray();  
        }

        $this->view->assign('continue_url',$continue_url); 
        $this->view->assign('images', $images);
    }
    
    public function gcsAction()
    {
        $resourcesTable = new Table_Resources();
        $qRequest = $this->getRequest()->getParam('q');
        $resourceResult = '';
        
        if (isset($qRequest) && !empty($qRequest)) {
            $resourceResult = $resourcesTable->getByKeyword($qRequest);
        }

        Zend_Layout::getMvcInstance()->getView()->q = $qRequest;
        $this->view->assign('resourceResult', $resourceResult);
    }
    
    public function pagesFacebookAction()
    {
        require_once('facebook-php-sdk-master/src/facebook.php');
        $fbConfig = new Zend_Config_Ini(APPLICATION_PATH.'/configs/config.ini', APPLICATION_ENV);
        $fbConfig = $fbConfig->toArray();
        $fbConfig = $fbConfig['facebook'];
        $facebook = new Facebook($fbConfig);
        
        $facebook_id = $this->getRequest()->getParam('facebook_id');
        $facebookFeed = $facebook->api('/'.$fbConfig['pageId'].'_'.$facebook_id.'');

        $pagesTable = new Table_Pages();
        $filesTable = new Table_Files();
        $resourcesTable = new Table_Resources();
        $categoriesTable = new Table_Categories();
        $zonesTable = new Table_Zones();
        
        $page = $pagesTable->getByFacebookId($facebook_id);
        if ($page === null || ($page['status'] != $pagesTable::STATUS_ACTIVE)) {
            $this->redirect('/menu');
        }
        
        $page = $page->toArray();
        $page['image'] = $filesTable->getById($page['image_id'])->toArray();
        $page['resource'] = $resourcesTable->getByCategoryIdAndKeyword($page['category_id'], $page['keyword']);
        $page['category'] = $categoriesTable->getById($page['category_id'])->toArray();
        $page['zone'] = $zonesTable->getById($page['category']['zone_id'])->toArray();
        $page['facebook'] = $facebookFeed;
        
        $lang = strtolower(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2));
        if (( $page['resource']['feed_lang'] != Table_Resources::FEED_LANG_ANY ) && ( $lang != $page['resource']['feed_lang'] )) {
            unset($page['resource']);
        }
        
        $this->view->assign('page', $page);
    }
        


}

