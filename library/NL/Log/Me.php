<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hpsese
 * Date: 21/09/11
 * Time: 13:03
 * To change this template use File | Settings | File Templates.
 */
 
class NL_Log_Me {

    /** @var $logger Zend_Log */
    private static $logger;

    // relative to APPLICATION_PATH
    private static $logFile = "public/logme.log";
    /**
     * @static
     * @param $message
     * @param int $priority
     * @param null $extras
     * @return void
     */
    static public function Log($message, $priority = Zend_Log::INFO, $extras = null){
        if( is_null(self::$logger) ){
            $logFile = realpath( APPLICATION_PATH . "/.." ). "/" . ltrim(self::$logFile,"/");
            self::$logger = new Zend_Log(new Zend_Log_Writer_Stream($logFile));
            self::$logger->setTimestampFormat("Y-m-d H:m");
        }

        ob_start();
        var_dump($message);
        $output = ob_get_clean();

        // neaten the newlines and indents
        $output = preg_replace('/\]\=\>\n(\s+)/m', '] => ', $output);
        
        self::$logger->log($output, $priority, $extras);
    }

    static public function setLogFile($logFile){
        self::$logFile = $logFile;
    }

}
