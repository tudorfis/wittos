<?php

 
class NL_AuthAdapter implements Zend_Auth_Adapter_Interface {

    protected $fbId;
    protected $userId;
    protected $username;
    protected $password;


    public function setUsernamePass( $username, $password ){
        $this->username = $username;
        $this->password = $password;
    }

    public function setFacebookId( $facebookId ){
        $this->fbId = $facebookId;
    }

    public function setUserId( $userId ){
        $this->userId = $userId;
    }

    /**
     * Performs an authentication attempt
     *
     * @throws Zend_Auth_Adapter_Exception If authentication cannot be performed
     * @return Zend_Auth_Result
     */
    public function authenticate()
    {
        $usersTable = new Table_Users();

        /** @var $user Model_User */
        if( $this->userId ){
            $user = $usersTable->getById($this->userId);
            $errMessage = "No user found with this User ID";
        }
        elseif( $this->fbId ){
            $user = $usersTable->getByFacebookId($this->fbId);
            $errMessage = "No user found with this Facebook ID";
        }
        else {
            $user = $usersTable->getAuthUsernamePassword($this->username, $this->password);
            $errMessage = "Username or password incorrect";
        }

        $code = Zend_Auth_Result::FAILURE;
        if( ! is_null( $user ) ){

            if( $user->getStatus() == Table_Users::STATUS_ENABLED ){

                // convert to std-object
                $oUser = (object) $user->toArray();

                $result = new Zend_Auth_Result(Zend_Auth_Result::SUCCESS, $oUser, array());
                return $result;
            }

            if( $user->getStatus() == Table_Users::STATUS_SUSPENDED ){
                $code = Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID;
                $errMessage = "Your account has been suspended. Please contact us for detail";
            }
            elseif($user->getStatus() == Table_Users::STATUS_DISABLED ){
                $errMessage = "Your account was not activated. Please check your email for activation link";
            }
        }


        $result = new Zend_Auth_Result($code, null, array($errMessage));
        return $result;
    }


    static protected function _getCookieParams(){
        $config = Zend_Registry::get("__CONFIG__");

        return array(
            "name" => isset( $config["cookie"]["name"] ) ?
                $config["cookie"]["name"] :
                $config["appnamespace"]."_id",

            "expires" => isset( $config["cookie"]["expires"] ) ?
                $config["cookie"]["expires"] : 14
        );
    }

    /**
     * Save Cookie. We pass parameter because the method is static
     *
     * @param $identityId
     */
    static public function saveCookie($identityId){
        $cookieParam = self::_getCookieParams();
        setcookie( $cookieParam["name"] , $identityId , time()+3600*24*$cookieParam["expires"],'/');
    }

    static public function clearCookie(){
        $cookieParam = self::_getCookieParams();
        setcookie( $cookieParam["name"],"",time()-1,'/');
    }

    /**
     * Check id in $_COOKIE array is the our cookie
     * @return bool
     */
    static public function getCookie(){
        $cookieParam = self::_getCookieParams();
        $cookieName = $cookieParam["name"];
        if( isset($_COOKIE[$cookieName]) ) {
            return intVal($_COOKIE[$cookieName]) > 0;
        }
        return false;
    }
}
