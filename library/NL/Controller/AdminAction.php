<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hpsese
 * Date: 23/09/11
 * Time: 17:42
 * To change this template use File | Settings | File Templates.
 */
 
class NL_Controller_AdminAction extends NL_Controller_Action {


    protected $adminNav;

    public function init()
    {
        /** @var Zend_Navigation $adminNav */
        $this->adminNav = Zend_Registry::get("admin-nav");

        $this->view->assign("adminMenu",$this->adminNav);
    }

}