<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hpsese
 * Date: 05/10/11
 * Time: 16:28
 * To change this template use File | Settings | File Templates.
 */

class NL_Controller_Plugin_Auth extends Zend_Controller_Plugin_Abstract
{
    private $_noAuth = array('module' => 'default', 'controller' => 'auth', 'action' => 'index');
    
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {

        $identity = null;
        if( Zend_Auth::getInstance()->hasIdentity() ){
            $identity = Zend_Auth::getInstance()->getIdentity();
        }
        elseif( $userId = NL_AuthAdapter::getCookie() ){
            // user id Exists
            $auth = Zend_Auth::getInstance();
            $authAdapter = new NL_AuthAdapter();

            $authAdapter->setUserId($userId);
            $result = $auth->authenticate($authAdapter);

            if( $result->isValid() ){
                $identity = Zend_Auth::getInstance()->getIdentity();
            }
        }

        $acl = NL_Acl::getInstance();

        $role = $identity ? $identity->role : "guest";

        $module = $request->getModuleName();
        if( is_null( $module ) ){
            $module = Zend_Controller_Front::getInstance()->getDefaultModule();
        }
        $controller = $request->getControllerName();
        $action = $request->getActionName();

        if( $controller == "error" ){
            // allow error controller
            return;
        }

        $resource = $module.":".$controller;
        if ( ! $acl->has( $resource ) ){
            throw new Exception("ACL Resource [".$resource."] undefined !",501);
        }

        if( ! $acl->isAllowed($role,$resource,$action) ){
//            throw new Exception("Not allowed to access [".$resource."] resource",501);
            $request->setModuleName($this->_noAuth['module']);
            $request->setControllerName($this->_noAuth['controller']);
            $request->setActionName($this->_noAuth['action']);
//            $request->setParam('controller', $this->_noAuth['controller']);
//            $request->setParam('view', 'no-auth');
        }
    }

}
