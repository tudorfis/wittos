<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hpsese
 * Date: 23/09/11
 * Time: 17:42
 * To change this template use File | Settings | File Templates.
 */
 
class NL_Controller_DefaultAction extends NL_Controller_Action {


    protected $userNav;

    public function init()
    {
        parent::init();

        if( $this->identity ){
            // don't use the user-navigation if no identity (is not loaded)
            /** @var Zend_Navigation $userNav */
        }

    }

}