<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hpsese
 * Date: 23/09/11
 * Time: 17:42
 * To change this template use File | Settings | File Templates.
 */
 
class NL_Controller_Action extends Zend_Controller_Action{

    protected $config;

    /** @var Zend_Layout */
    protected $layout;

    /** @var Zend_Controller_Action_Helper_ViewRenderer */
    protected $viewRenderer;

    protected $identity;
    protected $identityId;


    public function init()
    {
        $this->config = Zend_Registry::get("__CONFIG__");

        /** @var $layoutHelper Zend_Layout_Controller_Action_Helper_Layout */
        $layoutHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('layout');
        $this->layout = $layoutHelper->getLayoutInstance();

        /** @var $viewRenderer Zend_Controller_Action_Helper_ViewRenderer */
        $this->viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');

        if( Zend_Auth::getInstance()->hasIdentity() ){
            $this->identity = Zend_Auth::getInstance()->getIdentity();
            $this->identityId = $this->identity->id;
            $role = $this->identity->role;
        }
        else{
            $role = "guest";
        }

        $this->view->navigation()->setAcl(NL_Acl::getInstance())->setRole($role);
    }


    protected function assignBack(){
        $params = $this->getRequest()->getParams();

        $back = array();
        if( "index" != $params["action"] ){
            $back = array($params["controller"], $params["action"]);
        }
        elseif( "index" != $params["controller"] ){
            $back = array($params["controller"]);
        }

        if( isset($params["module"]) &&  "default" != $params["module"]){
            array_unshift($back , $params["module"]);
        }

        return implode("/",$back);
    }


    /**
     * Return the Request object
     *
     * @return Zend_Controller_Request_Http
     */
    public function getRequest()
    {
        return $this->_request;
    }


    protected function messageCenter( $viewName, array $params = array() ){
        $messageCenterNamespace = new Zend_Session_Namespace("messageCenter");

        $messageCenterNamespace->params = $params;

        //$this->redirect("/message/".$viewName);
        $this->_helper->redirector($viewName, "message", "default");
    }



    protected function disableLayout(){
        $this->layout->disableLayout();
        return $this;
    }

    protected function disableView($flag = true){
        $this->viewRenderer->setNoRender($flag);
        return $this;
    }

    protected function viewRenderer($viewScript){
        $this->viewRenderer($viewScript);
        return $this;
    }

    public function ajaxResponse($message = "", $success = 1, $data=null, $type = 'json')
    {
        $this->layout->disableLayout();
        $this->viewRenderer->setNoRender(true);

        if ( ! $success ){
            $message = "A problem occurred.";
        }

        echo Zend_Json::encode(array(
            'data' => $data,
            'success' => $success,
            'error' => !$success,
            'message' => $message,
            'type' => $type
        ));
        exit;
    }


    public function ajaxResponseSuccess($message = "", $data=null, $type = 'json'){
        $this->ajaxResponse($message, $success = 1, $data, $type);
    }

    public function ajaxResponseError($message = "", $data=null, $type = 'json'){
        $this->ajaxResponse($message, $success = 0, $data, $type);
    }


    /**
     * @param $action
     * @param null $controller
     * @param null $module
     * @param array $params
     * @return string
     */
    public function helperUrl($action, $controller = null, $module = null, array $params = null){
        return $this->_helper->url($action, $controller, $module, $params);
    }

    /**
     * wrapper for activation the menu navigation using findByOne() method
     * @param $property
     * @param $value
     */
    public function activateMenuBy($property,$value){
        $this->view->navigation()->findOneBy($property,$value)->setActive();
    }

}