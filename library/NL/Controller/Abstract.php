<?php

class NL_Controller_Abstract extends Zend_Controller_Action 
{
    
    protected function _makeTopMenu() 
    {
        $pagesTable = new Table_Pages();
        $categoriesTable = new Table_Categories();
        $zonesTable = new Table_Zones();
        $categoryWithPages = array(); 
        $menuLocations = array(); 
        
        $filter['order'] = 'ord ASC';
        $filter['status'] = $categoriesTable::STATUS_ACTIVE;
        $categoriesArray = $categoriesTable->getAll($filter)->toArray();
        $zonesArray = $zonesTable->getAll($filter)->toArray();
       
        
        foreach ($zonesArray as $zone) 
        {
            $categoriesByZones = $categoriesTable->getAllByZoneId($zone['id'])->toArray();
            if (!empty($categoriesByZones)) {    
                $menuLocations[$zone['id']]['zone'] = $zone;
                foreach ($categoriesByZones as $category) {
                    $pagesByCategories = $pagesTable->getAllByCategoryId($category['id'])->toArray();
                    if (!empty($pagesByCategories)) {
                       $menuLocations[$zone['id']]['categories'][$category['id']] = $category;
                       $menuLocations[$zone['id']]['categories'][$category['id']]['pages'] = $pagesByCategories;  
                    }
                }
                if(!isset($menuLocations[$zone['id']]['categories'])) {
                    unset($menuLocations[$zone['id']]);
                }
            }
        }
        
        Zend_Layout::getMvcInstance()->getView()->menuLocations = $menuLocations;
    }
        
    protected function _isWinner() 
    {
        $settingsTable = new Table_Settings();
        $numberOfVisits = $settingsTable->getByName('NUMBER_OF_VISITS')->toArray();
        $numberOfVisits = (int) $numberOfVisits['value'];
        $counterNumber = $settingsTable->getByName('COUNTER_NUMBER')->toArray();
        $counterNumber = (int) $counterNumber['value'];

        $settingsTable->update(
            array('value' => ++$numberOfVisits),
            array('name = ?' => 'NUMBER_OF_VISITS'));

        if (($numberOfVisits % $counterNumber) == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    protected function _makeGallery($gallery_id) 
    {
        $filesTable = new Table_Files();
        $galleryTable = new Table_Gallery();
        $images = array();
        
        $gallery = $galleryTable->getById($gallery_id);
        
        if (!empty($gallery)) 
        {
            $galleryArray = $gallery->toArray();
            $imagesIds = explode(',',$galleryArray['images']);
            foreach($imagesIds as $imageId) {
                $image = $filesTable->getById($imageId);
                $images[] = $image->toArray();
            }    
        }
        return $images;
    }
    
    protected function _generateWinCode() 
    {
        $generatedCode = substr(md5(rand(1,999999)), 0, 10);
       
        $codesTable = new Table_Codes();
        $data = array('code'      => $generatedCode);
        $codesTable->insert($data);
       
        return $generatedCode;
    }
    
    protected function _getZonesSelect()
    {
        $zonesTable = new Table_Zones();
        $filter['status'] = $zonesTable::STATUS_ACTIVE;
        $zonesArray =  $zonesTable->getAll($filter)->toArray();
        $zonesSelect = array();
        foreach($zonesArray as $zone) {
            $zonesSelect[$zone['id']] = $zone['name'];   
        }
        return $zonesSelect;
    }
    
    protected function _getCategoriesSelect()
    {
        $categoriesTable = new Table_Categories();
        $filter['status'] = $categoriesTable::STATUS_ACTIVE;
        $categoriesArray =  $categoriesTable->getAll($filter)->toArray();
        $categoriesSelect = array();
        foreach($categoriesArray as $category) {
            $categoriesSelect[$category['id']] = $category['title'];   
        }
        return $categoriesSelect;  
    }
    
    protected function _getStatusSelect()
    {
        return array('active' => 'Active',
                     'inactive' => 'Inactive');  
    }
    
    protected function _getFeedTypeSelect()
    {
        return array( Table_Resources::FEED_TYPE_FACEBOOK => Table_Resources::FEED_TYPE_FACEBOOK,
                       Table_Resources::FEED_TYPE_TWITTER => Table_Resources::FEED_TYPE_TWITTER,
                       Table_Resources::FEED_TYPE_WEBSITE => Table_Resources::FEED_TYPE_WEBSITE );
    }
    
    protected function _getFeedLangSelect()
    {
        return array( Table_Resources::FEED_LANG_EN => Table_Resources::FEED_LANG_EN,
                       Table_Resources::FEED_LANG_FR => Table_Resources::FEED_LANG_FR,
                       Table_Resources::FEED_LANG_ANY => Table_Resources::FEED_LANG_ANY );
    }
    
   
    
}
