<?php

class NL_HtmlMailer extends Zend_Mail
{

    // Chain emails
    const STYLES = "styles.phtml";
    // Auth Emails
    const SEND_TOKEN = 'send_token.phtml';
    const SEND_FORGOT_PASS_TOKEN = 'forgot_password.phtml';

    // contact
    const CONTACT_US = 'contact_us.phtml';

    /** @var @_view Zend_View */
    protected $_view;

    protected $viewScriptPath = '/views/scripts/email';

    protected $charset = 'utf8';

    protected $emailOption;

    protected $sendOption = 0;

    protected $domain;

    /**
     * @param string $charset
     */
    public function __construct($charset = "utf8")
    {

        $config = Zend_Registry::get("__CONFIG__");
        $this->emailOption = $config["email"];

        $this->charset = $charset;
        parent::__construct( $charset );
        $this->_view = new Zend_View();
        $this->_view->setScriptPath( APPLICATION_PATH . $this->viewScriptPath);

        /** @var $request Zend_Controller_Request_Http */
        $request = Zend_Controller_Front::getInstance()->getRequest();
        // set Default domain for link creation
        if( ! is_null($request) ){
            // set Default domain for link creation
            $this->domain = $request->getScheme() . '://' .$request->getHttpHost() . $request->getBaseUrl() ;
        }
    }

    public function setViewParam($property, $value)
    {
        $this->_view->assign($property, $value);
        return $this;
    }


    /**@return Zend_View */
    public function getView()
    {
        return $this->_view;
    }

    public function setDomain($domain)
    {
        $this->domain = $domain;
        return $this;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setViewScriptPath($viewScriptPath)
    {
        $this->viewScriptPath = APPLICATION_PATH .$viewScriptPath;
        return $this;
    }

    public function assign($key , $value){
        $this->_view->assign($key , $value);
        return $this;
    }

    /**
     * @param $template
     * @param string $encoding
     * @return bool
     */
    public function sendHtmlTemplate($template, $encoding = Zend_Mime::ENCODING_QUOTEDPRINTABLE)
    {
        try{
            $styles = $this->_view->render(self::STYLES);
        }
        catch( Exception $e ){
            $styles = "";
        }
        $html = $this->_view->render($template);
        $this->setBodyHtml($styles.$html, $this->getCharset(), $encoding);

        if ( ! $this->sendOption ){
            NL_Log_Me::Log("send==0 Dump: " . $template . "\n"
                          ."subject:" . $this->getSubject()."\n"
                          ."to:" . implode(",",$this->getRecipients())."\n");

            NL_Log_Me::Log($html);
            $this->clearRecipients();
            $this->clearSubject();
            return false;
        }

        $result = false;

        try {
            $this->send();

            $this->clearRecipients();
            $this->clearSubject();
            $result = true;
        } catch (Zend_Mail_Exception $e) {
            NL_Log_Me::Log($e->getMessage());
        }
        $log = $this->getDefaultTransport()->getConnection()->getLog();

        return $result;
    }


    /**
     * @throws Zend_Json_Exception
     * @param  string $keyName
     * @return void
     */
    protected function setEmailOptions( $keyName ){

        $this->sendOption = $this->emailOption["send_mail"];
        if( isset( $this->emailOption[$keyName] ) ){
            $this->setSubject($this->emailOption[$keyName]["subject"]);
            $this->sendOption &= $this->emailOption[$keyName]["send_mail"];
        }
        else{
            $this->setSubject($this->emailOption["subject"]);
        }
        //$this->setFrom($this->emailOption['from'], $this->emailOption['sender_name']);

        // domain is added default
        $this->assign("domain", $this->domain);
    }

    /**
     * @param Model_User $user
     * @param $token
     * @return bool
     */
    public function sendToken(Model_User $user, &$token )
    {
        $this->setEmailOptions("token");
        $template = self::SEND_TOKEN;

        $this->addTo($user->getEmail());

        $token = NL_Crypt::Encrypt( array("id" => $user->getId()) );
        $this->assign("user" , $user);
        $this->assign("token" , $token);

        return $this->sendHtmlTemplate($template, Zend_Mime::ENCODING_8BIT) ;
    }

    /**
     * @param Model_User $user
     * @param $token
     * @return bool
     */
    public function sendForgotPassToken(Model_User $user,  &$token)
    {
        $this->setEmailOptions("change_password");
        $template = self::SEND_FORGOT_PASS_TOKEN;

        $this->addTo($user->getEmail());

        $token = NL_Crypt::Encrypt( array("id"=>$user->getId()) );
        $this->assign("user" , $user);
        $this->assign("token" , $token);

        return $this->sendHtmlTemplate($template, Zend_Mime::ENCODING_8BIT) ;
    }


    public function sendContactUs($recipient,$name,$subject,$message,$userId){
        $this->setEmailOptions("contact_us");

        $template = self::CONTACT_US;

        $this->addTo($this->emailOption["webmaster"]);
        $this->setReplyTo($recipient,$name);

        $this->assign("email",$recipient);
        $this->assign("name",$name);
        $this->assign("subject",$subject);
        $this->assign("message",$message);

        return $this->sendHtmlTemplate($template, Zend_Mime::ENCODING_8BIT) ;
    }


    public function setInlineBanner(Model_Banner $banner)
    {
        $this->setType(Zend_Mime::MULTIPART_RELATED);

        $bannerAttach = $this->createAttachment(
            file_get_contents( APPLICATION_PATH . "/../public/images/banners/email/".$banner->getInfo() ),
            $banner->mimeByExtension(),
            Zend_Mime::DISPOSITION_INLINE,
            Zend_Mime::ENCODING_BASE64,
            $banner->getInfo()
        );

        $bannerAttach->id = "banner";
        return 1;
    }
}
