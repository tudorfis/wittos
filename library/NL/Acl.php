<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hpsese
 * Date: 05/10/11
 * Time: 15:57
 * To change this template use File | Settings | File Templates.
 */
 
class NL_Acl extends Zend_Acl {

    const ROLE_GUEST    = 'guest';
    const ROLE_USER     = 'user';
    const ROLE_ADMIN    = 'admin';

    /** Relative to APPLICATION_PATH  */
    const ACL_CONFIG  = "/configs/acl.php";

    /** @var $_instance NL_Acl */
    protected static $_instance;


    /**
     * @static
     * @return NL_Acl
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    /**
     * Singleton pattern
     */
    protected function __construct()
    {
        $this->getAclConfig();

        return $this;
    }

    /**
     * Include acl from configs/acl.php
     */
    protected function getAclConfig(){
        $aclConfigFile = realpath(  APPLICATION_PATH . self::ACL_CONFIG);
        if( ! file_exists($aclConfigFile) ){
            return;
        }

        $aclConfig = include_once( $aclConfigFile );

        if( ! array_key_exists("roles",$aclConfig ) ){
            // no roles in config, no cookies
            return;
        }

        // dump the ACL if debug = true
        if( ! empty($aclConfig["debug"] ) ){
            $this->dumpAcl($aclConfig);
        }

        // add roles
        foreach( $aclConfig["roles"] as $role => $parent ){
            $this->addRole(new Zend_Acl_Role($role), $parent );
        }

        $ruleTypeDef = array(self::TYPE_ALLOW,self::TYPE_DENY);

        foreach( $aclConfig["rules"] as $ruleType  => $roles ){

            $ruleTypeConst = "TYPE_".strtoupper($ruleType);
            assert( in_array($ruleTypeConst, $ruleTypeDef ) );

            foreach( $roles as $role => $rpPairs ){
                if( $role == "all"){
                    $role = null;
                }
                elseif( ! $this->hasRole( $role ) ){
                    // undeclared role - create it
                    $this->addRole(new Zend_Acl_Role( $role ));
                }
                foreach( $rpPairs as $resource => $privileges ){
                    if( ! $this->has($resource) ){
                        $this->addResource(new Zend_Acl_Resource($resource));
                    }
                    $this->setRule(self::OP_ADD,$ruleTypeConst,$role,$resource,$privileges);
                }
            }
        }
    }

    /**
     * Log all ACLs create by config file.
     *
     * @param $aclConfig
     */
    protected function dumpAcl($aclConfig){
        $addRole = '$this->addRole(new Zend_Acl_Role("%s"),%s);';
        $addResource = '$this->addResource(new Zend_Acl_Resource("%s"));';
        $addRule = '$this->setRule(self::OP_ADD,"%s","%s","%s",%s)';

        $buffer = array(
            "roles" => array(),
            "resources" => array(),
            "rules" => array()
        );
        // container for created resources, to mimic $this->hasResource();
        $resources = array();
        // add roles
        foreach( $aclConfig["roles"] as $role => $parent ){
            // $this->addRole(new Zend_Acl_Role($role), $parent );
            $buffer["roles"][] = sprintf($addRole,$role,is_null($parent) ? 'null' : '"'.$parent.'"');
        }

        $ruleTypeDef = array(self::TYPE_ALLOW,self::TYPE_DENY);

        foreach( $aclConfig["rules"] as $ruleType  => $roles ){

            $ruleTypeConst = "TYPE_".strtoupper($ruleType);
            assert( in_array($ruleTypeConst, $ruleTypeDef ) );

            foreach( $roles as $role => $rpPairs ){
                if( $role == "all"){
                    $role = null;
                }
                elseif( ! array_key_exists($role,$aclConfig["roles"]) ){
                    // undeclared role - create it
                    // $this->addRole(new Zend_Acl_Role( $role ));
                    $buffer["roles"][] = sprintf($addRole,$role,'null');
                }
                foreach( $rpPairs as $resource => $privileges ){
                    if( ! in_array( $resource,$resources ) ){
                        // $this->addResource(new Zend_Acl_Resource($resource));
                        $resources[] = $resource;
                        $buffer["resources"][] = sprintf($addResource,$resource);
                    }
                    //$this->setRule(self::OP_ADD,$ruleTypeConst,$role,$resource,$privileges);
                    $buffer["rules"][] = sprintf($addRule,$ruleTypeConst,$role,$resource,
                        is_null($privileges) ? 'null' : '"'.$privileges.'"'
                    );
                }
            }
        }
        NL_Log_Me::Log($buffer);
    }

}
