-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2013 at 11:17 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wittos`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `ord` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `zone_id`, `title`, `description`, `status`, `ord`) VALUES
(1, 1, 'Racing', 'Racing', 'active', 1),
(2, 2, 'Hybrid', 'Hybrid', 'active', 2),
(6, 1, 'Pit stop', 'Pit stop', 'active', 3);

-- --------------------------------------------------------

--
-- Table structure for table `codes`
--

CREATE TABLE IF NOT EXISTS `codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `status` enum('accepted','rejected','pending') DEFAULT 'pending',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `codes`
--

INSERT INTO `codes` (`id`, `code`, `status`, `date_created`) VALUES
(2, 'dd7c06eb76', 'accepted', '2013-09-16 10:07:52'),
(3, 'd55318effd', 'rejected', '2013-09-16 10:09:49'),
(4, 'asd124sr34', 'rejected', '2013-09-05 21:00:00'),
(5, 'bc55d3363e', 'pending', '2013-09-16 11:58:07'),
(6, '0a65a2060d', 'pending', '2013-09-16 11:58:23'),
(7, '07493d1f6b', 'pending', '2013-09-16 12:05:40'),
(8, '296b80a37b', 'pending', '2013-09-16 12:05:53'),
(9, '9a276dadf7', 'pending', '2013-09-16 12:06:16'),
(10, '6fe3448db2', 'pending', '2013-09-16 13:19:52'),
(11, 'eee3545b78', 'pending', '2013-09-17 07:02:43'),
(12, '829329992a', 'pending', '2013-09-17 14:56:52'),
(13, '0f6d5a63d5', 'pending', '2013-09-17 15:20:49'),
(14, 'cc81e73d77', 'pending', '2013-09-18 07:01:50'),
(15, '8cd1dda689', 'pending', '2013-09-18 07:36:05'),
(16, '77d6dc03c6', 'pending', '2013-09-18 10:50:47'),
(17, '879f983462', 'pending', '2013-09-26 15:34:11'),
(18, '870797b09e', 'pending', '2013-09-27 07:16:44'),
(19, '1e3edf92d8', 'pending', '2013-09-27 07:17:38'),
(20, '95b61b034f', 'pending', '2013-09-27 07:18:01'),
(21, '372516fe16', 'pending', '2013-09-27 07:20:22'),
(22, '1257e1a6e0', 'pending', '2013-09-27 07:23:18'),
(23, 'b37cbeb47f', 'pending', '2013-09-27 07:24:11'),
(24, 'a17379f02b', 'pending', '2013-09-27 07:29:37'),
(25, '078aed9de1', 'pending', '2013-09-27 08:12:28'),
(26, '54082c93dc', 'pending', '2013-09-27 08:24:24'),
(27, 'c848782160', 'pending', '2013-09-27 08:28:48'),
(28, '9c2310187e', 'pending', '2013-09-27 08:30:47'),
(29, 'c82cd1b827', 'pending', '2013-09-27 08:31:57'),
(30, 'acdf90cf34', 'pending', '2013-09-27 08:33:14'),
(31, '3f7bca0503', 'pending', '2013-09-27 08:34:00'),
(32, 'e404a0d6be', 'pending', '2013-09-27 08:40:01'),
(33, '29dcc53d18', 'pending', '2013-09-27 08:40:35'),
(34, '0d3fccd2ab', 'pending', '2013-09-27 08:43:27'),
(35, '124fb19d40', 'pending', '2013-09-27 08:44:38'),
(36, '66cff14a77', 'pending', '2013-09-27 08:46:05'),
(37, '4bef8c26b9', 'pending', '2013-09-27 08:49:55'),
(38, '21144ac020', 'pending', '2013-09-27 08:51:54'),
(39, '55e0aca90d', 'pending', '2013-09-27 08:52:18'),
(40, 'f5cd9b59bd', 'pending', '2013-09-27 09:29:32'),
(41, '19d06e3a1a', 'pending', '2013-09-27 09:29:33'),
(42, '07c8735ef9', 'pending', '2013-09-27 15:26:14'),
(43, 'ed96e1cac5', 'pending', '2013-09-27 15:26:16'),
(44, '2ebd7d2957', 'pending', '2013-09-30 13:17:33'),
(45, '1bd89c2aa2', 'pending', '2013-09-30 13:17:34'),
(46, 'f1ed4b874c', 'pending', '2013-10-02 14:39:37'),
(47, 'c41c540a57', 'pending', '2013-10-02 14:39:39'),
(48, 'e8b9a1e451', 'pending', '2013-10-02 15:06:55'),
(49, '9166aba45e', 'pending', '2013-10-02 16:08:10'),
(50, 'cbf5d23e0b', 'pending', '2013-10-02 16:08:31'),
(51, '5a89dad602', 'pending', '2013-10-02 16:09:01'),
(52, '165e5d79ba', 'pending', '2013-10-02 16:11:31'),
(53, '84262a902e', 'pending', '2013-10-02 16:11:55'),
(54, '57783bea61', 'pending', '2013-10-02 16:12:11'),
(55, '5670acff72', 'pending', '2013-10-02 16:24:24'),
(56, '6c2d8ddaff', 'pending', '2013-10-03 16:48:47'),
(57, 'c5ea68b14a', 'pending', '2013-10-04 07:09:32'),
(58, '8a2f78d8ee', 'pending', '2013-10-09 14:48:17');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) DEFAULT NULL,
  `path_thumb` varchar(255) DEFAULT NULL,
  `file_type` enum('static','facebook') NOT NULL DEFAULT 'static',
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=280 ;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `path`, `path_thumb`, `file_type`, `status`, `date_created`) VALUES
(1, 'images/lemans_g1.jpg', '', 'static', 'active', '2013-09-13 12:03:39'),
(2, 'images/lemans_g2.jpg', '', 'static', 'active', '2013-09-13 12:03:44'),
(3, 'images/lemans_g3.jpg', '', 'static', 'active', '2013-09-13 12:04:16'),
(4, 'images/lemans_g4.jpg', '', 'static', 'active', '2013-09-13 12:04:16'),
(5, 'images/lemans_g5.jpg', '', 'static', 'active', '2013-09-13 12:04:16'),
(6, 'images/lemans_g6.jpg', '', 'static', 'active', '2013-09-13 12:04:16'),
(7, 'images/lemans_g2_1.jpg', '', 'static', 'active', '2013-10-02 16:00:41'),
(8, 'images/lemans_g2_2.jpg', '', 'static', 'active', '2013-10-02 16:01:05'),
(229, 'http://photos-e.ak.fbcdn.net/hphotos-ak-ash3/1379880_10151935176941798_826915451_n.jpg', 'http://photos-e.ak.fbcdn.net/hphotos-ak-ash3/1379880_10151935176941798_826915451_s.jpg', 'facebook', 'active', '2013-10-01 13:10:55'),
(230, 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn2/1377098_10151925350161798_727210614_n.jpg', 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn2/1377098_10151925350161798_727210614_s.jpg', 'facebook', 'active', '2013-09-26 11:09:44'),
(231, 'http://photos-e.ak.fbcdn.net/hphotos-ak-ash3/12558_10151806925716798_213849700_n.jpg', 'http://photos-e.ak.fbcdn.net/hphotos-ak-ash3/12558_10151806925716798_213849700_s.jpg', 'facebook', 'active', '2013-08-02 13:08:55'),
(232, '_n', 'http://external.ak.fbcdn.net/safe_image.php?d=AQD7Zy2r8bAS6YtT&w=154&h=154&url=http%3A%2F%2Fwww.mativi-champs-elysees.fr%2F%2Fmedias%2Fimages%2Fchamps-elysees%2Fthumbs%2F2410-51c9bf3223eb0.jpg', 'facebook', 'active', '2013-07-25 11:07:25'),
(233, 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn1/17528_10151755334046798_244332692_n.jpg', 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn1/17528_10151755334046798_244332692_s.jpg', 'facebook', 'active', '2013-07-09 10:07:48'),
(234, 'http://photos-f.ak.fbcdn.net/hphotos-ak-prn1/1011799_10151747373771798_769302074_n.jpg', 'http://photos-f.ak.fbcdn.net/hphotos-ak-prn1/1011799_10151747373771798_769302074_s.jpg', 'facebook', 'active', '2013-07-05 10:07:35'),
(235, '_n', 'http://external.ak.fbcdn.net/safe_image.php?d=AQC3zPMMd5XgCz2n&w=154&h=154&url=https%3A%2F%2Ffbcdn-sphotos-d-a.akamaihd.net%2Fhphotos-ak-prn1%2Fq71%2Fs720x720%2F1026244_10151733717731798_697712687_o.jpg', 'facebook', 'active', '2013-06-28 11:06:32'),
(236, 'http://photos-a.ak.fbcdn.net/hphotos-ak-ash3/7751_10151727876736798_492936010_n.jpg', 'http://photos-a.ak.fbcdn.net/hphotos-ak-ash3/7751_10151727876736798_492936010_s.jpg', 'facebook', 'active', '2013-06-28 05:06:33'),
(237, 'http://photos-c.ak.fbcdn.net/hphotos-ak-prn1/1017481_10151718465481798_169066299_n.jpg', 'http://photos-c.ak.fbcdn.net/hphotos-ak-prn1/1017481_10151718465481798_169066299_s.jpg', 'facebook', 'active', '2013-06-20 13:06:18'),
(238, 'http://photos-h.ak.fbcdn.net/hphotos-ak-prn1/1012103_10151707295631798_1309412200_n.jpg', 'http://photos-h.ak.fbcdn.net/hphotos-ak-prn1/1012103_10151707295631798_1309412200_s.jpg', 'facebook', 'active', '2013-06-24 17:06:59'),
(239, 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn2/954703_10151702349101798_576911383_n.png', 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn2/954703_10151702349101798_576911383_s.png', 'facebook', 'active', '2013-06-14 13:06:42'),
(240, '_n', 'http://scontent-b.xx.fbcdn.net/hphotos-prn1/s480x480/993512_537666119624039_1446709604_n.png', 'facebook', 'active', '2013-06-11 17:06:58'),
(241, 'http://photos-g.ak.fbcdn.net/hphotos-ak-ash4/1004580_10151699105506798_696690619_n.jpg', 'http://photos-g.ak.fbcdn.net/hphotos-ak-ash4/1004580_10151699105506798_696690619_s.jpg', 'facebook', 'active', '2013-06-10 13:06:05'),
(242, '_n', 'http://external.ak.fbcdn.net/safe_image.php?d=AQD7Zy2r8bAS6YtT&w=154&h=154&url=http%3A%2F%2Fwww.mativi-champs-elysees.fr%2F%2Fmedias%2Fimages%2Fchamps-elysees%2Fthumbs%2F2410-51c9bf3223eb0.jpg', 'facebook', 'active', '2013-07-25 11:07:25'),
(243, '_n', 'http://external.ak.fbcdn.net/safe_image.php?d=AQC3zPMMd5XgCz2n&w=154&h=154&url=https%3A%2F%2Ffbcdn-sphotos-d-a.akamaihd.net%2Fhphotos-ak-prn1%2Fq71%2Fs720x720%2F1026244_10151733717731798_697712687_o.jpg', 'facebook', 'active', '2013-06-28 11:06:32'),
(244, 'http://photos-a.ak.fbcdn.net/hphotos-ak-ash3/7751_10151727876736798_492936010_n.jpg', 'http://photos-a.ak.fbcdn.net/hphotos-ak-ash3/7751_10151727876736798_492936010_s.jpg', 'facebook', 'active', '2013-06-28 05:06:33'),
(245, 'http://photos-h.ak.fbcdn.net/hphotos-ak-ash3/14001_546462655387022_1027117260_n.jpg', 'http://photos-h.ak.fbcdn.net/hphotos-ak-ash3/14001_546462655387022_1027117260_s.jpg', 'facebook', 'active', '2013-02-25 08:02:36'),
(246, 'http://photos-e.ak.fbcdn.net/hphotos-ak-ash2/538186_10151505412786798_498791704_n.jpg', 'http://photos-e.ak.fbcdn.net/hphotos-ak-ash2/538186_10151505412786798_498791704_s.jpg', 'facebook', 'active', '2013-02-19 11:02:18'),
(247, 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn1/16327_10151505405256798_618991101_n.jpg', 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn1/16327_10151505405256798_618991101_s.jpg', 'facebook', 'active', '2013-02-19 10:02:20'),
(248, 'http://photos-a.ak.fbcdn.net/hphotos-ak-ash3/7751_10151727876736798_492936010_n.jpg', 'http://photos-a.ak.fbcdn.net/hphotos-ak-ash3/7751_10151727876736798_492936010_s.jpg', 'facebook', 'active', '2013-06-28 05:06:33'),
(249, 'http://photos-c.ak.fbcdn.net/hphotos-ak-prn1/1017481_10151718465481798_169066299_n.jpg', 'http://photos-c.ak.fbcdn.net/hphotos-ak-prn1/1017481_10151718465481798_169066299_s.jpg', 'facebook', 'active', '2013-06-20 13:06:18'),
(250, 'http://photos-h.ak.fbcdn.net/hphotos-ak-prn1/1012103_10151707295631798_1309412200_n.jpg', 'http://photos-h.ak.fbcdn.net/hphotos-ak-prn1/1012103_10151707295631798_1309412200_s.jpg', 'facebook', 'active', '2013-06-24 17:06:59'),
(251, 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn2/954703_10151702349101798_576911383_n.png', 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn2/954703_10151702349101798_576911383_s.png', 'facebook', 'active', '2013-06-14 13:06:42'),
(252, '_n', 'http://scontent-b.xx.fbcdn.net/hphotos-prn1/s480x480/993512_537666119624039_1446709604_n.png', 'facebook', 'active', '2013-06-11 17:06:58'),
(253, 'http://photos-g.ak.fbcdn.net/hphotos-ak-ash4/1004580_10151699105506798_696690619_n.jpg', 'http://photos-g.ak.fbcdn.net/hphotos-ak-ash4/1004580_10151699105506798_696690619_s.jpg', 'facebook', 'active', '2013-06-10 13:06:05'),
(254, 'http://photos-c.ak.fbcdn.net/hphotos-ak-ash4/789_10151681228746798_1212205712_n.jpg', 'http://photos-c.ak.fbcdn.net/hphotos-ak-ash4/789_10151681228746798_1212205712_s.jpg', 'facebook', 'active', '2013-05-31 13:05:40'),
(255, 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn1/532876_10151623478576798_799978434_n.jpg', 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn1/532876_10151623478576798_799978434_s.jpg', 'facebook', 'active', '2013-04-26 15:04:37'),
(256, 'http://photos-e.ak.fbcdn.net/hphotos-ak-ash3/1379880_10151935176941798_826915451_n.jpg', 'http://photos-e.ak.fbcdn.net/hphotos-ak-ash3/1379880_10151935176941798_826915451_s.jpg', 'facebook', 'active', '2013-10-10 14:10:51'),
(257, 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn2/1377098_10151925350161798_727210614_n.jpg', 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn2/1377098_10151925350161798_727210614_s.jpg', 'facebook', 'active', '2013-09-26 11:09:44'),
(258, 'http://photos-e.ak.fbcdn.net/hphotos-ak-ash3/1379880_10151935176941798_826915451_n.jpg', 'http://photos-e.ak.fbcdn.net/hphotos-ak-ash3/1379880_10151935176941798_826915451_s.jpg', 'facebook', 'active', '2013-10-10 14:10:51'),
(259, 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn2/1377098_10151925350161798_727210614_n.jpg', 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn2/1377098_10151925350161798_727210614_s.jpg', 'facebook', 'active', '2013-09-26 11:09:44'),
(260, 'http://photos-e.ak.fbcdn.net/hphotos-ak-ash3/12558_10151806925716798_213849700_n.jpg', 'http://photos-e.ak.fbcdn.net/hphotos-ak-ash3/12558_10151806925716798_213849700_s.jpg', 'facebook', 'active', '2013-08-02 13:08:55'),
(261, 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn1/17528_10151755334046798_244332692_n.jpg', 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn1/17528_10151755334046798_244332692_s.jpg', 'facebook', 'active', '2013-07-09 09:07:48'),
(262, 'http://photos-f.ak.fbcdn.net/hphotos-ak-prn1/1011799_10151747373771798_769302074_n.jpg', 'http://photos-f.ak.fbcdn.net/hphotos-ak-prn1/1011799_10151747373771798_769302074_s.jpg', 'facebook', 'active', '2013-07-05 09:07:35'),
(263, 'http://photos-a.ak.fbcdn.net/hphotos-ak-ash3/7751_10151727876736798_492936010_n.jpg', 'http://photos-a.ak.fbcdn.net/hphotos-ak-ash3/7751_10151727876736798_492936010_s.jpg', 'facebook', 'active', '2013-06-28 04:06:33'),
(264, 'http://photos-c.ak.fbcdn.net/hphotos-ak-prn1/1017481_10151718465481798_169066299_n.jpg', 'http://photos-c.ak.fbcdn.net/hphotos-ak-prn1/1017481_10151718465481798_169066299_s.jpg', 'facebook', 'active', '2013-06-20 12:06:18'),
(265, 'http://photos-h.ak.fbcdn.net/hphotos-ak-prn1/1012103_10151707295631798_1309412200_n.jpg', 'http://photos-h.ak.fbcdn.net/hphotos-ak-prn1/1012103_10151707295631798_1309412200_s.jpg', 'facebook', 'active', '2013-06-24 16:06:59'),
(266, 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn2/954703_10151702349101798_576911383_n.png', 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn2/954703_10151702349101798_576911383_s.png', 'facebook', 'active', '2013-06-14 12:06:42'),
(267, '_n', 'http://sphotos-f.ak.fbcdn.net/hphotos-ak-prn1/s480x480/993512_537666119624039_1446709604_n.png', 'facebook', 'active', '2013-06-11 16:06:58'),
(268, 'http://photos-g.ak.fbcdn.net/hphotos-ak-ash4/1004580_10151699105506798_696690619_n.jpg', 'http://photos-g.ak.fbcdn.net/hphotos-ak-ash4/1004580_10151699105506798_696690619_s.jpg', 'facebook', 'active', '2013-06-10 12:06:05'),
(269, 'http://photos-c.ak.fbcdn.net/hphotos-ak-ash4/789_10151681228746798_1212205712_n.jpg', 'http://photos-c.ak.fbcdn.net/hphotos-ak-ash4/789_10151681228746798_1212205712_s.jpg', 'facebook', 'active', '2013-05-31 12:05:40'),
(270, 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn1/532876_10151623478576798_799978434_n.jpg', 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn1/532876_10151623478576798_799978434_s.jpg', 'facebook', 'active', '2013-04-26 14:04:37'),
(271, 'http://photos-e.ak.fbcdn.net/hphotos-ak-ash3/1379880_10151935176941798_826915451_n.jpg', 'http://photos-e.ak.fbcdn.net/hphotos-ak-ash3/1379880_10151935176941798_826915451_s.jpg', 'facebook', 'active', '2013-10-10 14:10:51'),
(272, 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn2/1377098_10151925350161798_727210614_n.jpg', 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn2/1377098_10151925350161798_727210614_s.jpg', 'facebook', 'active', '2013-09-26 11:09:44'),
(273, 'http://photos-c.ak.fbcdn.net/hphotos-ak-prn1/1017481_10151718465481798_169066299_n.jpg', 'http://photos-c.ak.fbcdn.net/hphotos-ak-prn1/1017481_10151718465481798_169066299_s.jpg', 'facebook', 'active', '2013-06-20 13:06:18'),
(274, 'http://photos-h.ak.fbcdn.net/hphotos-ak-prn1/1012103_10151707295631798_1309412200_n.jpg', 'http://photos-h.ak.fbcdn.net/hphotos-ak-prn1/1012103_10151707295631798_1309412200_s.jpg', 'facebook', 'active', '2013-06-24 17:06:59'),
(275, 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn2/954703_10151702349101798_576911383_n.png', 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn2/954703_10151702349101798_576911383_s.png', 'facebook', 'active', '2013-06-14 13:06:42'),
(276, '_n', 'http://scontent-b.xx.fbcdn.net/hphotos-prn1/s480x480/993512_537666119624039_1446709604_n.png', 'facebook', 'active', '2013-06-11 17:06:58'),
(277, 'http://photos-g.ak.fbcdn.net/hphotos-ak-ash4/1004580_10151699105506798_696690619_n.jpg', 'http://photos-g.ak.fbcdn.net/hphotos-ak-ash4/1004580_10151699105506798_696690619_s.jpg', 'facebook', 'active', '2013-06-10 13:06:05'),
(278, 'http://photos-c.ak.fbcdn.net/hphotos-ak-ash4/789_10151681228746798_1212205712_n.jpg', 'http://photos-c.ak.fbcdn.net/hphotos-ak-ash4/789_10151681228746798_1212205712_s.jpg', 'facebook', 'active', '2013-05-31 13:05:40'),
(279, 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn1/532876_10151623478576798_799978434_n.jpg', 'http://photos-a.ak.fbcdn.net/hphotos-ak-prn1/532876_10151623478576798_799978434_s.jpg', 'facebook', 'active', '2013-04-26 15:04:37');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` varchar(255) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `images`, `status`, `date_created`, `date_modified`) VALUES
(1, '1,2,3,4,5,6', 'active', '2013-09-13 00:00:00', '2013-09-13 00:00:00'),
(2, '1,2', 'active', '2013-10-02 18:59:49', '0000-00-00 00:00:00'),
(3, '7,8', 'active', '2013-10-02 19:01:22', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `image_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `facebook_id` varchar(255) DEFAULT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  `ord` int(11) DEFAULT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'inactive',
  `page_type` enum('static','facebook') NOT NULL DEFAULT 'static',
  `slug` varchar(255) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=280 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `content`, `image_id`, `category_id`, `gallery_id`, `facebook_id`, `keyword`, `ord`, `status`, `page_type`, `slug`, `date_created`, `date_modified`) VALUES
(227, 'YARIS HYBRID-R concept', 'C''est bien le concept YARIS HYBRID-R qui est arrivé hier. Une voiture de rêve dédiée au plaisir de conduite. Deux moteurs électriques à l’arrière, un moteur 1.6L turbo  "de course" à l’avant.  Soit un système hybride totalisant 420ch. Sportive sur route comme sur piste. Ca y est, vous souriez!<br />\r\n<br />\r\n// It is indeed the YARIS HYBRID-R concept now on display at Le Rendez-Vous Toyota. An exciting dream car dedicated to maximizing driving pleasure. Two electric motors at the rear and a 1.6 turbo Global Race Engine at the front. Together the hybrid system delivers 420ch. A sports car that maximises sensations on road or on track. That''s it, you’re smiling!<br />\r\n', 229, 2, NULL, '10151935444806798', 'hybrid', 6, 'active', 'facebook', NULL, '2013-10-01 16:10:55', '0000-00-00 00:00:00'),
(228, 'Yaris', 'Dès lundi, un nouveau concept arrive au Rendez-Vous Toyota et va mettre un sourire sur votre visage. Devinez-vous lequel c''est ?<br />\r\n<br />\r\n// From next Monday, a new concept arrives at Le Rendez-Vous Toyota and will put a smile on your face. Can you guess which one it is?', 230, 2, NULL, '10151925350186798', 'yaris hybrid', 7, 'inactive', 'facebook', NULL, '2013-09-26 14:09:44', '0000-00-00 00:00:00'),
(229, 'Toyota & 24h du Mans', 'Toyota et la passion du sport...en images!<br />\r\n<br />\r\n// Toyota and the passion for sport...enjoy the photos !', 231, 1, NULL, '10151806947901798', 'competition pitstop', 8, 'active', 'facebook', NULL, '2013-08-02 16:08:55', '0000-00-00 00:00:00'),
(231, 'GT86', 'The Toyota GT86 and 2000GT sports cars @Le Rendez-Vous Toyota this summer', 233, 1, NULL, '10151755454391798', 'racing', 10, 'active', 'facebook', NULL, '2013-07-09 10:07:48', '0000-00-00 00:00:00'),
(232, 'Pit stop', 'En promenade ce week-end sur les Champs-Elysées? Arrêtez-vous au Rendez-Vous Toyota pour notre dernière session de pit stop et gagnez peut-être des entrées pour le Festival de Goodwood la semaine ! RDV samedi entre 14h et 18h.', 234, 6, NULL, '10151747373791798', 'pit stop', 11, 'active', 'facebook', NULL, '2013-07-05 10:07:35', '0000-00-00 00:00:00'),
(248, 'TEST', '', 248, 1, NULL, '10151727876851798', 'test', 12, 'active', 'facebook', NULL, '2013-06-28 05:06:33', '0000-00-00 00:00:00'),
(273, NULL, 'Le groupe VIOLIN FEVER revient au Rendez-Vous Toyota pour célébrer la fête de la musique. Vibrez au son de leurs violons électriques sur des musiques lounge, pop ou rock!<br />\r\nRDV DEMAIN VENDREDI 21 JUIN ENTRE 21H ET 23H.', 273, NULL, NULL, '10151718465591798', NULL, 13, 'inactive', 'facebook', NULL, '2013-06-20 13:06:18', '0000-00-00 00:00:00'),
(274, NULL, 'Notre nouvelle exposition part sur les chapeaux de roue:<br />\r\n<br />\r\nCE WEEK-END 2 SESSIONS DE PITSTOP CHALLENGE SAMEDI 15 ET DIMANCHE 16 JUIN ENTRE 14H ET 18H AU RENDEZ-VOUS TOYOTA.<br />\r\n<br />\r\nA GAGNER: DES ENTREES AUX 24 HEURES DU MANS ! (comprenant transferts, hébergement et accès à l''hospitalité Toyota les samedi 22 et dimanche 23 juin prochains - règlement complet au Rendez-Vous Toyota).<br />\r\n<br />\r\nTels les mécaniciens de la team Toyota Hybrid Racing, venez changer une roue de TS030 en un temps record… Un vrai travail d’équipe et une course contre la montre !<br />\r\n<br />\r\nUne autre occasion pour les perdants de notre jeu Endurance Clic (ici: http://j.mp/ToyotaEnduranceClic), de tenter leur chance !', 274, NULL, NULL, '10151707295656798', NULL, 14, 'inactive', 'facebook', NULL, '2013-06-24 17:06:59', '0000-00-00 00:00:00'),
(275, NULL, 'Amis sportifs, bonjour! Notre prochaine exposition mettra la course automobile à l''honneur et à l''occasion des 90 ans des 24 Heures du Mans, Toyota déroule le "tapis rouge" à ses fans Facebook et organise un jeu pour aller soutenir son équipe au Mans!<br />\r\n<br />\r\nVous pensez avoir autant d''endurance que la team Toyota Racing ?<br />\r\nVenez nous le prouver sur http://j.mp/ToyotaEnduranceClic et peut-être remporter vos places pour les 24 heures !<br />\r\n<br />\r\nLe principe de l''Endurance Clic est simple, celui qui reste le plus longtemps cliqué sur sa souris gagne !<br />\r\n<br />\r\nLes 200 premiers à tenter leur chance pourront même assister au départ de la course au Rendez-Vous Toyota. <br />\r\n<br />\r\nAlors tous à vos souris !', 275, NULL, NULL, '10151702349116798', NULL, 15, 'inactive', 'facebook', NULL, '2013-06-14 13:06:42', '0000-00-00 00:00:00'),
(276, 'Timeline Photos', 'Les 24 Heures du Mans arrivent à grands pas ! <br />\r\nVous pensez avoir autant d''endurance que la team Toyota Hybride Racing ?<br />\r\nVenez nous le prouver sur http://j.mp/ToyotaEnduranceClic et peut être remporter vos places pour les 24 heures !<br />\r\n<br />\r\nLe principe de l''Endurance Clic est simple, celui qui reste le plus longtemps cliqué sur sa souris gagne !<br />\r\n<br />\r\nLes 200 premiers à tenter leur chance pourront même assister au départ de la course au Le Rendez-Vous Toyota', 276, NULL, NULL, '10151701195951798', NULL, 16, 'inactive', 'facebook', NULL, '2013-06-11 17:06:58', '0000-00-00 00:00:00'),
(277, NULL, 'Quelques jours et puis Toyota ME.WE s''en va... Dès jeudi, un coup de maguette magique (ou presque!) et elle laissera place à de belles sportives. Fin du suspense avec la réouverture de nos portes vendredi après-midi. <br />\r\n<br />\r\nEt pour nos fidèles de Facebook fan de sport auto, une surprise bientôt en ligne vous attend: restez connectés!', 277, NULL, NULL, '10151699105521798', NULL, 17, 'inactive', 'facebook', NULL, '2013-06-10 13:06:05', '0000-00-00 00:00:00'),
(278, NULL, 'Curieux de voir comment une idée de concept automobile prend forme en 3D? Venez percer les secrets du design automobile avec nos graphistes présents au Rendez-Vous Toyota ce week-end et toute la semaine prochaine (de 15h à 19h).', 278, NULL, NULL, '10151681228756798', NULL, 18, 'inactive', 'facebook', NULL, '2013-05-31 13:05:40', '0000-00-00 00:00:00'),
(279, 'Family exhibition', 'Parce que le Printemps donne des envies de virées à la campagne, Le Rendez-Vous Toyota met sa gamme familiale à l’honneur. Les nouveaux Verso, Auris, RAV4 et Prius+, conçus pour les « tribus », offrent confort, modularité et dernières technologies pour davantage de plaisir et de bien-être. Sur la mezzanine, un concept unique Toyota attend plus particulièrement les enfants : la Camatte. Ce véhicule 3 places qui peut être conduit par un enfant, est destiné à apporter une expérience à la fois ludique et pédagogique de la conduite en favorisant les échanges parents-enfant. // With Spring here at last and everyone raring to go for a drive in the country, Le Rendez-Vous Toyota gives pride of place to its range of family cars. Designed with “the whole family” in mind, the new Verso, Auris, RAV4 and Prius+ offer comfort, flexibility and the latest technology for your greater enjoyment and well-being. On the mezzanine floor, a unique Toyota concept awaits the kids in particular: the Camatte. This 3-seater vehicle, which can actually be driven by a child, aims to offer a fun but educational driving experience and encourage parent-child communication.', 279, NULL, NULL, '10151623520266798', NULL, 19, 'inactive', 'facebook', NULL, '2013-04-26 15:04:37', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

CREATE TABLE IF NOT EXISTS `resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) DEFAULT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'inactive',
  `link` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `category_id` int(11) DEFAULT NULL,
  `feed_type` enum('facebook','twitter','website') DEFAULT NULL,
  `feed_lang` enum('en','fr','any') DEFAULT 'any',
  `video_show` tinyint(1) NOT NULL DEFAULT '0',
  `video_link` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `resources`
--

INSERT INTO `resources` (`id`, `keyword`, `status`, `link`, `title`, `description`, `category_id`, `feed_type`, `feed_lang`, `video_show`, `video_link`, `date_created`) VALUES
(1, 'racing', 'active', 'http://www.toyotahybridracing.com/toyota-racing-in-focus-at-frankfurt-motor-show/', 'TOYOTA Racing in focus at Frankfurt Motor Show', 'A 2012-spec TS030 HYBRID joined a unique display of its hybrid powertrain – the only display example in the world...', 1, 'website', 'en', 0, '', '2013-10-09 11:41:49'),
(2, 'racing', 'active', 'http://www.toyotahybridracing.com/toyota-racing-in-focus-at-frankfurt-motor-show/', 'TS030 HYBRID', 'Introduction at the 65th Frankfurt International Motor Show -IAA-\r\nWatch the video now...', 1, 'website', 'any', 0, '', '2013-10-09 11:41:49'),
(8, 'pit stop', 'deleted', 'http://www.toyota-europe.com/', 'pit stop page', '...', 1, 'facebook', 'en', 0, '', '2013-10-09 11:41:49'),
(10, 'hybrid', 'active', 'http://www.youtube.com/watch?v=OU2rxV4HUtY&feature=c4-overview-vl&list=PLMJ_oV58WE4ETXzXRygbNuKruEVeTphYh', 'Yaris Hybrid-R concept - Frankfurt Motor Show 2013', 'Based on the Yaris 3-door, the Yaris Hybrid R concept hybrid powertrain configuration, employs a powerful and highly tuned 1.6l petrol engine combined with two powerful electric motors to provide an ''intelligent'' electric four-wheel drive capability. ', 2, 'website', 'en', 0, '', '2013-10-09 11:41:49'),
(19, 'competition', 'active', 'http://www.toyota-europe.com/rendezvous/articles/dream_car_contest_win.tmex', 'COMPETITION: 7th Toyota Dream Car ', 'European shortlisted drawings will be submitted to the jury of Toyota Japan among all the participating countries...', 2, 'website', 'en', 0, '', '2013-10-10 10:21:10'),
(22, 'asd', 'deleted', 'http://www.youtube.com/', 'asdasd', 'sadasd', 2, 'twitter', 'fr', 0, '', '2013-10-10 13:08:46'),
(23, 'racing', 'active', 'http://www.toyota.fr/about/news_and_events/toyota_racing_podium_mans.tmex', 'Toyota Racing monte sur le podium du Mans', 'TOYOTA Racing a signé une émouvante deuxième place sur la 90ème édition des 24 Heures du Mans...', 1, 'website', 'fr', 0, '', '2013-10-14 16:24:09'),
(24, 'racing', 'active', 'https://www.facebook.com/photo.php?fbid=591890547534929&set=a.147362158654439.28906.119431368114185&type=1', 'Toyota GT86', 'On fait crisser les pneus, on joue avec le frein main et on appuie sur l’accélérateur ! Voici le drift le plus rapide du monde avec ses 217 km/h...', 1, 'facebook', 'fr', 0, '', '2013-10-14 16:45:30');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'NUMBER_OF_VISITS', '135'),
(2, 'COUNTER_NUMBER', '4'),
(3, 'SHOW_ZONE_ID', '1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `first_name` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `last_name` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `role` enum('user','admin') COLLATE utf8_bin DEFAULT NULL,
  `fb_id` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `status` enum('enabled','disabled','suspended') COLLATE utf8_bin DEFAULT 'disabled',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ukEmail` (`username`),
  KEY `kPassword` (`password`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `role`, `fb_id`, `status`, `date_created`) VALUES
(1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'Todorescu', 'Tudor', 'admin', NULL, 'enabled', '2013-09-12 05:35:15');

-- --------------------------------------------------------

--
-- Table structure for table `zones`
--

CREATE TABLE IF NOT EXISTS `zones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `ord` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `zones`
--

INSERT INTO `zones` (`id`, `name`, `status`, `ord`) VALUES
(1, 'Ground Floor', 'active', 0),
(2, '1st Floor', 'active', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
